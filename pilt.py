import cv2
import cv
import numpy as np
import abi
import camera_settings
import subprocess
import pickle


class Pilt:
    BALL_MORPH_KERNEL_SIZE = 1  # dilate-erode jaoks
    MIN_BALL_SIZE = 5
    MAX_BALL_SIZE = 5000
    MIN_GOAL_SIZE = 300
    LINE_CONST = 10  # musta ja valge punkti max kaugus joone jaoks
    COL_FILE = '/home/spd13/sdp13/colors_hsv_fixed.txt'
    COL_FILE_PICKLE = '/home/spd13/sdp13/colors_hsv.pkl'

    def __init__(self, cam_id, varav_id):
        self.cap = cv2.VideoCapture(cam_id)
        self.frame_hsv = None  # yhe frame masiiv
        self.frame_bgr = None
        self.px_width = 800  #640
        self.px_height = 448  #480
        self.seadista_camera()
        self.varav_id = varav_id
        self.pall_hsv = [0, 0, 0]
        self.pall_dev = [0, 0, 0]
        self.varav_hsv = [0, 0, 0]
        self.varav_dev = [0, 0, 0]
        self.oma_varav_hsv = [0, 0, 0]
        self.oma_varav_dev = [0, 0, 0]
        self.black_hsv = [0, 0, 0]
        self.black_dev = [0, 0, 0]
        self.white_hsv = [0, 0, 0]
        self.white_dev = [0, 0, 0]
        self.viimati_nahtud_varav = 'vastane'  # oma / vastane
        self.viimati_nahtud_v_x = self.px_width // 2
        # self.load_pall_hsv()
        # self.load_varav_hsv(varav_id)
        self.load_cols(varav_id)

    def __del__(self):
        cv2.destroyAllWindows()
        self.cap.release()

    def seadista_camera(self):
        if self.cap.isOpened():
            self.cap.set(cv.CV_CAP_PROP_FRAME_WIDTH, self.px_width)
            self.cap.set(cv.CV_CAP_PROP_FRAME_HEIGHT, self.px_height)
            self.cap.set(cv.CV_CAP_PROP_FPS, 30)

            try:  # seaded, mida opencv ei lase, auto whitebalance off!
                camera_settings.set_cam_settings_from_cli()
            except subprocess.CalledProcessError as e:
                print("Jama kaamera seadistamisega CLI kaudu.")
                print(e.output)

    def set_pall_hsv(self, hsv, dev):
        self.pall_hsv = hsv
        self.pall_dev = dev

    def set_varav_hsv_by_id(self, hsv, dev, v_id):
        if v_id == self.varav_id:
            self.varav_hsv = hsv
            self.varav_dev = dev
        else:
            self.oma_varav_hsv = hsv
            self.oma_varav_dev = dev

    def load_cols(self, v_id):
        with open(Pilt.COL_FILE_PICKLE) as f:
            cols = pickle.load(f)
        self.pall_hsv, self.pall_dev = cols['orange']
        self.black_hsv, self.black_dev = cols['black']
        self.white_hsv, self.white_dev = cols['white']

        self.varav_id = v_id
        if v_id == 0:  # kollane
            self.varav_hsv, self.varav_dev = cols['yellow']
            self.oma_varav_hsv, self.oma_varav_dev = cols['blue']
        else:
            self.varav_hsv, self.varav_dev = cols['blue']
            self.oma_varav_hsv, self.oma_varav_dev = cols['yellow']

    def load_pall_hsv(self):  # loeb failist palli varvid
        with open(Pilt.COL_FILE, 'r') as f:
            lines = f.read().splitlines()
        self.pall_hsv = map(int, lines[0].split()[1:4])
        self.pall_dev = map(int, lines[0].split()[4:7])

    def load_varav_hsv(self, v_id):  # loeb failist varava varvid, id = 0 (kollane) voi 1 (sinine)
        with open(Pilt.COL_FILE, 'r') as f:
            _lines = f.read().splitlines()
        self.varav_id = v_id
        if v_id == 0:
            oma_varav_id = 1
        else:
            oma_varav_id = 0

        self.varav_hsv = map(int, _lines[v_id + 1].split()[1:4])
        self.varav_dev = map(int, _lines[v_id + 1].split()[4:7])
        self.oma_varav_hsv = map(int, _lines[oma_varav_id + 1].split()[1:4])
        self.oma_varav_dev = map(int, _lines[oma_varav_id + 1].split()[4:7])

    def set_frame(self):  # loeb kaamerast pildi ja teisendab hsv'ks
        if self.cap.isOpened:
            self.frame_bgr = self.cap.read()[1]
            self.frame_hsv = cv2.cvtColor(self.frame_bgr, cv2.COLOR_BGR2HSV)

    def get_frame_hsv(self):
        return self.frame_hsv

    def get_frame_bgr(self):
        return self.frame_bgr

    def get_px_width(self):
        return self.px_width

    def get_px_height(self):
        return self.px_height

    @staticmethod
    def get_contours_frame(img, hsv, dev):  # tagastab leitud kujundid, sorteeritud suurima jargi
        hsv_array = np.array(hsv)
        hsv_dev_array = np.array(dev)

        lowerb = hsv_array - hsv_dev_array
        upperb = hsv_array + hsv_dev_array

        dst = np.zeros(img.shape[:2], np.uint8)  # tulemuse hoidmiseks
        cv2.inRange(img, lowerb, upperb, dst)  # tekitab mustvalge pildi (dst) sobivatest punktidest

        kernel = np.ones((Pilt.BALL_MORPH_KERNEL_SIZE, Pilt.BALL_MORPH_KERNEL_SIZE), np.uint8)
        dst = cv2.morphologyEx(dst, cv2.MORPH_CLOSE, kernel)  # dilate + erode
        #cv2.imshow('blq', dst)
        contours, hier = cv2.findContours(dst, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)  # leiame kujundid
        contours.sort(key=cv2.contourArea, reverse=True)

        return contours

    @staticmethod
    def get_contours_frame_pall(img, hsv, dev):  # siin siis lisa tresh hue jaoks, 2kki vaja?
        hsv_array = np.array(hsv)
        hsv_dev_array = np.array(dev)

        lowerb = hsv_array - hsv_dev_array
        upperb = hsv_array + hsv_dev_array
        lowerb2 = np.array([180, lowerb[1], lowerb[2]])  # hue ylevalt ka
        upperb2 = np.array([255, upperb[1], upperb[2]])

        dst = np.zeros(img.shape[:2], np.uint8)  # tulemuse hoidmiseks
        dst2 = np.zeros(img.shape[:2], np.uint8)
        cv2.inRange(img, lowerb, upperb, dst)  # tekitab mustvalge pildi (dst) sobivatest punktidest
        cv2.inRange(img, lowerb2, upperb2, dst2)
        cv2.bitwise_or(dst, dst2, dst)

        kernel = np.ones((Pilt.BALL_MORPH_KERNEL_SIZE, Pilt.BALL_MORPH_KERNEL_SIZE), np.uint8)
        dst = cv2.morphologyEx(dst, cv2.MORPH_CLOSE, kernel)  # dilate + erode
        # cv2.imshow('blq', dst)
        contours, hier = cv2.findContours(dst, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)  # leiame kujundid
        contours.sort(key=cv2.contourArea, reverse=True)

        return contours

    def pallid(self):  # peab tagastame pallide jarjendi, sobivaim eesmpool
        # TODO pallid() - hetkel tagastab ainult suurima palli...praegu OK
        pallid = []
        # self.set_frame()  # votame pildi
        # uus jupp, leiab varava ning paneb mustaks selle bloki, oluline, et votaksime pilti kaamerast 1 korra..
        ###########
        # varav_vaiksemaks_alt = 10
        goal = self.varav()
        if goal:
            self.viimati_nahtud_varav = 'vastane'
            x, y, dx, dy = goal[0]
            self.viimati_nahtud_v_x = x + dx//2
            # dy = max(10, dy - 10)
            # cv2.rectangle(self.frame_hsv, (x,y), (x+dx,y+dy), [0,0,0], cv.CV_FILLED)
            # cv2.rectangle(self.frame_bgr, (x,y), (x+dx,y+dy), [0,0,0], cv.CV_FILLED)

        oma_goal = self.oma_varav()
        if oma_goal:
            self.viimati_nahtud_varav = 'oma'
            x, y, dx, dy = oma_goal[0]
            self.viimati_nahtud_v_x = x + dx//2
            # cv2.rectangle(self.frame_hsv, (x,y), (x+dx,y+dy), [0,0,0], cv.CV_FILLED)
            # cv2.rectangle(self.frame_bgr, (x,y), (x+dx,y+dy), [0,0,0], cv.CV_FILLED)
        ##################

        contours = Pilt.get_contours_frame(self.frame_hsv, self.pall_hsv, self.pall_dev)

        # kui 2 ysna yhe suurust contouri, siis valime, mis on keskpunktile l2hemal
        def nurgake(objekt):
            return abs(abi.kaugus(objekt, self.px_width, self.px_height)[1])

        if contours and Pilt.MIN_BALL_SIZE <= cv2.contourArea(contours[0]) <= Pilt.MAX_BALL_SIZE:
            suuruse_piir = cv2.contourArea(contours[0]) * (1 - 0.3)

            for contour in contours:
                if cv2.contourArea(contour) >= suuruse_piir:
                    uus_pall = cv2.boundingRect(contour)
                    if self.sobib(uus_pall):
                        pallid.append(uus_pall)

            pallid.sort(key=nurgake)

        if pallid:  # naitame bgr pildil
            x, y, dx, dy = pallid[0]
            cv2.circle(self.frame_bgr, (x+dx//2, y+dy//2), 5, [0, 0, 255], cv.CV_FILLED)
            kaugus, nurk = abi.kaugus(pallid[0], self.px_width, self.px_height)
            cv2.putText(self.frame_bgr, "pall: " + str(round(kaugus, 2)), (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 1, [0, 0, 255], 2)

        return pallid

    @staticmethod
    def get_joonelt_max(joon, low, high):
        l1, l2, l3 = low
        h1, h2, h3 = high

        # treshime joone punktid (millegip2rast inRange ei toota)
        joon_tresh = (joon[:, 0] >= l1)*(joon[:, 0] < h1) * (joon[:, 1] >= l2)*(joon[:, 1] < h2) * (joon[:, 2] >= l3)*(joon[:, 2] < h3)

        # tagastame suurima indeksi, kus v22rtused langesid sisse
        try:
            return len(joon_tresh) - np.where(joon_tresh[::-1])[0][0] - 1
        except IndexError:
            return -1

    @staticmethod
    def get_joonelt_min(joon, low, high):
        l1, l2, l3 = low
        h1, h2, h3 = high

        # treshime joone punktid (millegip2rast inRange ei toota)
        joon_tresh = (joon[:, 0] >= l1)*(joon[:, 0] < h1) * (joon[:, 1] >= l2)*(joon[:, 1] < h2) * (joon[:, 2] >= l3)*(joon[:, 2] < h3)

        # tagastame v2hima indeksi, kus v22rtused langesid sisse
        try:
            return np.where(joon_tresh)[0][0]
        except IndexError:
            return -1

    def sobib(self, pall):  # kas pall on valjakul (ei ole piiridest valjas)
        # return True
        must_low, must_high = [a - b for a, b in zip(self.black_hsv, self.black_dev)], [a + b for a, b in zip(self.black_hsv, self.black_dev)]
        valge_low, valge_high = [a - b for a, b in zip(self.white_hsv, self.white_dev)], [a + b for a, b in zip(self.white_hsv, self.white_dev)]
        # roh_low, roh_high = (0, 0, 0), (10, 10, 10)
        x, y, dx, dy = pall
        ys = np.arange(y + dy, self.px_height)
        xs = np.linspace(x + dx / 2, self.px_width // 2, num=len(ys)).astype('uint16')

        for i in (-2, -1, 0, 1, 2):  # teeme viis k5rvuti asetsevat joont l2bi
            joone_punktid = self.frame_hsv[ys, xs + i]

            # joon algab pallist ning tuleb meie poole, esmalt must, siis valge joon
            max_musta_indeks = Pilt.get_joonelt_max(joone_punktid, must_low, must_high)
            # print 'max_must: ', max_musta_indeks
            if max_musta_indeks > 0:
                # see on siis alates max_musta_indeks 'st
                min_valge_indeks = Pilt.get_joonelt_min(joone_punktid[max_musta_indeks:, ], valge_low, valge_high)
                # print('min_valge: ', min_valge_indeks)
                if 0 < min_valge_indeks < Pilt.LINE_CONST:  # leiti ning piisavalt l2hedal
                    return False

        return True

    def get_joon_y(self):
        # teeme 2 joont, m5lemast ylemisest nyrgast alla keskele
        # kas kummagi joonel on must/valge joon, leida max indeks sellele
        joon_y_vas = 0  # kui ei leia, siis tagastame 0
        joon_y_par = 0

        must_low, must_high = [a - b for a, b in zip(self.black_hsv, self.black_dev)], [a + b for a, b in zip(self.black_hsv, self.black_dev)]
        valge_low, valge_high = [a - b for a, b in zip(self.white_hsv, self.white_dev)], [a + b for a, b in zip(self.white_hsv, self.white_dev)]

        ys = np.arange(1, self.px_height)
        xs1 = np.linspace(1, self.px_width // 2, num=len(ys)).astype('uint16')  # vasakult
        xs2 = np.linspace(self.px_width - 1, self.px_width // 2, num=len(ys)).astype('uint16')  # paremalt

        joone_punktid_vasak = self.frame_hsv[ys, xs1]
        joone_punktid_parem = self.frame_hsv[ys, xs2]

        # vasak joon
        max_musta_indeks = Pilt.get_joonelt_max(joone_punktid_vasak, must_low, must_high)

        if max_musta_indeks > 0:
            # see on siis alates max_musta_indeks 'st
            min_valge_indeks = Pilt.get_joonelt_min(joone_punktid_vasak[max_musta_indeks:, ], valge_low, valge_high)
            # print('min_valge: ', min_valge_indeks)
            if 0 < min_valge_indeks < Pilt.LINE_CONST:  # leiti ning piisavalt l2hedal
                joon_y_vas = max_musta_indeks

        # parem joon
        max_musta_indeks = Pilt.get_joonelt_max(joone_punktid_parem, must_low, must_high)

        if max_musta_indeks > 0:
            # see on siis alates max_musta_indeks 'st
            min_valge_indeks = Pilt.get_joonelt_min(joone_punktid_parem[max_musta_indeks:, ], valge_low, valge_high)
            # print('min_valge: ', min_valge_indeks)
            if 0 < min_valge_indeks < Pilt.LINE_CONST:  # leiti ning piisavalt l2hedal
                joon_y_par = max_musta_indeks

        return max(joon_y_vas, joon_y_par)

    def pall_vasakul_joonel(self):
        pall_low, pall_high = [a - b for a, b in zip(self.pall_hsv, self.pall_dev)], [a + b for a, b in zip(self.pall_hsv, self.pall_dev)]
        ys = np.arange(1, int(self.px_height * 0.9))
        xs = np.linspace(self.px_width // 2, self.px_width // 2 - self.px_width // 18, num=len(ys)).astype('uint16')  # vasakult

        joone_punktid = self.frame_hsv[ys, xs]

        pall_indeks = Pilt.get_joonelt_min(joone_punktid, pall_low, pall_high)

        return pall_indeks > 0

    def pall_paremal_joonel(self):
        pall_low, pall_high = [a - b for a, b in zip(self.pall_hsv, self.pall_dev)], [a + b for a, b in zip(self.pall_hsv, self.pall_dev)]
        ys = np.arange(1, int(self.px_height * 0.9))
        xs = np.linspace(self.px_width // 2, self.px_width // 2 + self.px_width // 18, num=len(ys)).astype('uint16')  # paremalt

        joone_punktid = self.frame_hsv[ys, xs]

        pall_indeks = Pilt.get_joonelt_min(joone_punktid, pall_low, pall_high)

        return pall_indeks > 0

    def varav(self):  # leiab pildilt varava, tagastab
        # TODO varav() - hetkel naiivne, leiab lihtsalt boundingRecti ymber suurima kontuuri
        varav = []
        # self.set_frame()  # votame pildi

        contours = self.get_contours_frame(self.frame_hsv, self.varav_hsv, self.varav_dev)

        if contours and cv2.contourArea(contours[0]) >= Pilt.MIN_GOAL_SIZE:
            varav.append(cv2.boundingRect(contours[0]))

        if varav:
            kaugus, nurk = abi.kaugus(varav[0], self.px_width, self.px_height)
            cv2.putText(self.frame_bgr, "varav: " + str(round(kaugus, 2)), (10, 90), cv2.FONT_HERSHEY_SIMPLEX, 1, [0, 0, 255], 2)

        return varav

    def oma_varav(self):
        varav = []

        contours = self.get_contours_frame(self.frame_hsv, self.oma_varav_hsv, self.oma_varav_dev)

        if contours and cv2.contourArea(contours[0]) >= Pilt.MIN_GOAL_SIZE:
            varav.append(cv2.boundingRect(contours[0]))

        if varav:
            kaugus, nurk = abi.kaugus(varav[0], self.px_width, self.px_height)
            cv2.putText(self.frame_bgr, "oma varav: " + str(round(kaugus, 2)), (10, 120), cv2.FONT_HERSHEY_SIMPLEX, 1, [0, 0, 255], 2)

        return varav

    def varav_by_id(self, v_id):  # leiab pildilt varava, tagastab
        varav = []
        self.set_frame()  # votame pildi

        if v_id == self.varav_id:
            contours = self.get_contours_frame(self.frame_hsv, self.varav_hsv, self.varav_dev)
        else:
            contours = self.get_contours_frame(self.frame_hsv, self.oma_varav_hsv, self.oma_varav_dev)

        if contours and cv2.contourArea(contours[0]) >= Pilt.MIN_GOAL_SIZE:
            varav.append(cv2.boundingRect(contours[0]))
        # print varav
        return varav

    # def pallid2(self):  # netist leitud, HughCircles versioon - ei toota vist paremini, parameetritega m2ng
    #     pallid = []
    #     self.set_frame()
    #
    #     hsv_array = np.array(self.pall_hsv)
    #     hsv_dev_array = np.array(self.pall_dev)
    #
    #     lowerb = hsv_array - hsv_dev_array
    #     upperb = hsv_array + hsv_dev_array
    #     lowerb2 = np.array([170, lowerb[1], lowerb[2]])  # hue ylevalt ka
    #     upperb2 = np.array([255, upperb[1], upperb[2]])
    #
    #     dst1 = np.zeros(self.frame_hsv.shape[:2], np.uint8)  # tulemuse hoidmiseks
    #     dst2 = np.zeros(self.frame_hsv.shape[:2], np.uint8)  # tulemuse hoidmiseks
    #     cv2.inRange(self.frame_hsv, lowerb, upperb, dst1)  # tekitab mustvalge pildi (dst) sobivatest punktidest
    #     cv2.inRange(self.frame_hsv, lowerb2, upperb2, dst2)  # tekitab mustvalge pildi (dst) sobivatest punktidest
    #     cv2.bitwise_or(dst1, dst2, dst1)  # or-me kokku
    #
    #     # kernel = np.ones((Pilt.BALL_MORPH_KERNEL_SIZE, Pilt.BALL_MORPH_KERNEL_SIZE), np.uint8)
    #     # cv2.morphologyEx(dst1, cv2.MORPH_CLOSE, kernel, dst1)
    #     cv2.GaussianBlur(dst1, (9, 9), 2, dst1)  # v5ib proovida pilti smoothida nii - neti variandis oli seda tehtud
    #     cv2.imshow('blq', dst1)
    #     circles = cv2.HoughCircles(dst1, cv.CV_HOUGH_GRADIENT, 2, 50, param1=200, param2=50)  # TODO, kas ok??
    #
    #     def nurgake(objekt):
    #         return abs(abi.kaugus(objekt, self.px_width, self.px_height)[1])
    #
    #     if circles is not None and len(circles[0]) > 0:
    #         # print('circles!!')
    #         circles = circles[0]
    #         circles = list(circles)
    #         circles.sort(key=lambda x:x[2], reverse=True)  # raadiuse j2rgi
    #         suuruse_piir = circles[0][2] * (1 - 0.3)
    #
    #         for circle in circles:
    #             # print circle
    #             x, y, r = circle
    #             if r >= suuruse_piir:
    #                 uus_pall = (int(x - r), int(y - r), int(2*r), int(2*r))  # tekitame nelinurga samal kujul kui boundingBox
    #                 if self.sobib(uus_pall):
    #                     pallid.append(uus_pall)
    #
    #         pallid.sort(key=nurgake)
    #
    #     if pallid:  # naitame bgr pildil
    #         x, y, dx, dy = pallid[0]
    #         cv2.circle(self.frame_bgr, (x+dx//2, y+dy//2), 5, [0, 0, 255], cv.CV_FILLED)
    #         kaugus, nurk = abi.kaugus(pallid[0], self.px_width, self.px_height)
    #         cv2.putText(self.frame_bgr, "pall: " + str(round(kaugus, 2)), (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 1, [0, 0, 255], 2)
    #
    #     return pallid

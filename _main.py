# juhtkood
from liikumine import Liikumine  # mootorite juhtimine
from pilt import Pilt  # kaamerast pilt ja asjad
from coilgun import Coilgun
import abi  # abi funktsioonid arvutamiseks
import time
import cv2
import camera_settings

# seaded
palli_otsimise_kiirus = 10  # default otsimise suund(+/-) ja kiirus
max_otsi_lisa = 40  # palju kiirendame max v5rreldes eelmisega n5ksutamisel
noksu_periood, noksu_tasa = 10, 4  # n5ksutamise periood ning min kiirusel liikumise aeg (tsyklites)

otse_liikumise_kiirus = 160  # otse liikumise kiirus
keeramise_kiirus = 25 #20
palliga_keeramise_kiirus = 60
suus_kiirus = 25
kadumiseAeg = 1  # aeg mille jooksul pildist kadunud pall, mis ei ole j6udnud driblerisse, lugeda kadunuks

loogika1_timout = 3
loogika35_timeout = 2
loogika4_timout = 4
loogika45_timout = 2
loogika5_aeg_enne_looki = 0.2

seisund_counter = 0  # kiirendamise jaoks

goal_id = 0  # 0 - kollane, 1 - sinine

#mootorite id-d
vasak = 1
parem = 2
taga = 3

#coilguni id
coil = 4

#muutujad
aeg = 0
timeout_counter = 0  # yritame kasutada yhte muutujat
loogika1_counter = 0  # n5ksuga keeramise jaoks
v_kaugus = 0
oma_v_kaugus = 0

#loogika: 1 - otsin palli; 2 - liigun pallile; 3 - pall kohe dribleris aga pildilt kohe v2ljas;
# 4 - pall dribleris; 5 - l88n
#loogika TODO: 0 - nuppude asjad : loogika -2 - muuda parameetrid vastavaks
loogika = 1  # mm..lyliteid pole
loogika_abi = 0  # printimisel abiks, prindime ainult muutused

#kaamera ja mootorite init
yhendus = abi.yhendused([vasak, parem, taga, coil])
mootorid = yhendus[0:3]
s_coil = yhendus[3]

kaamera = Pilt(0, goal_id)
mootor = Liikumine(mootorid)
coilgun = Coilgun(s_coil)

joone_max_y = kaamera.get_px_height() * 0.6  # min joone kauguse m22ramiseks

coilgun.dribbler_stop()


# lisame trackbari v2rava valimiseks
def on_trackbar(tb_value):
    global goal_id
    try:
        goal_id = tb_value
        kaamera.load_cols(goal_id)
        print 'varava ID:', goal_id
    except:
        print 'Viga varava ID muutmisega'


def on_trackbar_2(tb_value):
    camera_settings.set_cam_settings_from_cli()


def on_trackbar_3(tb_value):
    coilgun.change_dribbler_state()

switch = '0 : Kollane\n1 : Sinine'
switch1 = 'Trigger cam settings'
switch2 = 'Dribbler stop/run'
cv2.namedWindow('Kaamera pilt')
cv2.createTrackbar(switch, 'Kaamera pilt', goal_id, 1, on_trackbar)
cv2.createTrackbar(switch1, 'Kaamera pilt', 0, 1, on_trackbar_2)
cv2.createTrackbar(switch2, 'Kaamera pilt', 0, 1, on_trackbar_3)
# trackari osa lopeb

try:
    while True: #kood v6iks pidevalt joosta
        try:  # kuvab pilti, mida kaamera naeb. v2ike overhead on pildi konvertimisel hetkel
            # bgr_img = cv2.cvtColor(kaamera.get_frame_hsv(), cv2.COLOR_HSV2BGR)
            bgr_img = kaamera.get_frame_bgr()
            cv2.putText(bgr_img, "Varava ID: " + str(goal_id), (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, [0, 0, 255], 2)
            cv2.putText(bgr_img, "Loogika: " + str(loogika), (100, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, [0, 0, 255], 2)
            cv2.putText(bgr_img, "last_goal: " + str(kaamera.viimati_nahtud_varav), (100, 90), cv2.FONT_HERSHEY_SIMPLEX, 1, [0, 0, 255], 2)
            cv2.putText(bgr_img, "last_goal_x: " + str(kaamera.viimati_nahtud_v_x), (100, 120), cv2.FONT_HERSHEY_SIMPLEX, 1, [0, 0, 255], 2)
            cv2.imshow('Kaamera pilt', bgr_img)
            ch = cv2.waitKey(5)
            if ch == 1048689 or ch == 1048603 or ch == ord('q'):  # 'q' or 'Esc'
                break
        except:
            print 'probleem pildi naitamisel'

        if loogika != loogika_abi:  # prindime muutused v2lja
            print "loogika: ", loogika
            loogika_abi = loogika

        coilgun.ping()  # pingime, loodetavasti pole probleem, et nii tihti
        kaamera.set_frame()  # igas tsyklis anname kaamerale pildi

        if abi.pall_dribleris(yhendus[1]) and loogika not in (35, 4, 45, 5, -2):
            loogika = 35
            timeout_counter = 0
            loogika1_counter = 0

        if loogika == -2:
            if abi.get_lyliti_state(yhendus[2], 0): #v2rava valik, muuda parameetrid vastavaks
                goal_id = 0
            else:
                goal_id = 1
            kaamera.load_cols(goal_id)
            print 'varava ID:', goal_id

            if abi.get_lyliti_state(yhendus[0], 0): #v6isttluskoodi start, muuda parameetrid vastavaks
                loogika = 1
            else:
                time.sleep(0.3)

        elif loogika == -1:
            varav = kaamera.varav()
            oma_varav = kaamera.oma_varav()
            seisund_counter += 1

            if varav:
                otse_kiirus = min(otse_liikumise_kiirus, 20 + 3*seisund_counter)
                mootor.otse(otse_kiirus)
                #mootor.otse(otse_liikumise_kiirus)
                if abi.kaugus(varav[0], kaamera.get_px_width(), kaamera.get_px_height()) < otse_kaugus:
                    loogika = 1
                    seisund_counter = 0
            elif oma_varav:
                otse_kiirus = min(otse_liikumise_kiirus, 20 + 3*seisund_counter)
                mootor.otse(otse_kiirus)
                #mootor.otse(otse_liikumise_kiirus)
                if abi.kaugus(oma_varav[0], kaamera.get_px_width(), kaamera.get_px_height()) < otse_kaugus:
                    loogika = 1
                    seisund_counter = 0
            else:
                loogika = 1
                seisund_counter = 0

        elif loogika == 0:
            varav = kaamera.varav()
            oma_varav = kaamera.oma_varav()

            if v_kaugus != 0 and oma_v_kaugus != 0:
                if v_kaugus > oma_v_kaugus:  # rynnatav v2rav kaugemal kui oma
                    if varav:
                        # v2rav on pildi keskel, oleme 5iges suunas l88gi jaoks
                        if abi.keskel(varav[0], kaamera.get_px_width(), kaamera.get_px_height(), 10):
                            loogika = -1  # otse edasi
                            print "LOOGIKA -1 !!"
                            otse_kaugus = v_kaugus // 3
                        else:
                            nurk = abi.kaugus(varav[0], kaamera.get_px_width(), kaamera.get_px_height())[1]
                            mootor.keera_nurgaga(nurk, palli_otsimise_kiirus + 30)

                    else:
                        # mootor.keera(suund)
                        keera_kiirus = abi.noksu_f(loogika1_counter, noksu_periood,
                                           noksu_tasa, palli_otsimise_kiirus, max_otsi_lisa)
                        mootor.keera_konst_paremale(keera_kiirus)
                        #mootor.keera_ymber_dribbleri(palli_otsimise_kiirus)
                else:
                    if oma_varav:
                        # v2rav on pildi keskel, oleme 5iges suunas l88gi jaoks
                        if abi.keskel(oma_varav[0], kaamera.get_px_width(), kaamera.get_px_height(), 10):
                            loogika = -1  # otse edasi
                            print "LOOGIKA -1 !!"
                            otse_kaugus = oma_v_kaugus // 3
                        else:
                            nurk = abi.kaugus(oma_varav[0], kaamera.get_px_width(), kaamera.get_px_height())[1]
                            mootor.keera_nurgaga(nurk, palli_otsimise_kiirus + 30)
                    else:
                        # mootor.keera(suund)
                        #mootor.keera_ymber_dribbleri(palli_otsimise_kiirus)
                        keera_kiirus = abi.noksu_f(loogika1_counter, noksu_periood,
                                           noksu_tasa, palli_otsimise_kiirus, max_otsi_lisa)
                        mootor.keera_konst_paremale(keera_kiirus)

            elif varav and v_kaugus == 0:  # pole veel v2ravat pildis, keerame ringi
                # v2rav on pildi keskel, oleme 5iges suunas l88gi jaoks
                if abi.keskel(varav[0], kaamera.get_px_width(), kaamera.get_px_height(), 10):
                    v_kaugus, nurk = abi.kaugus(varav[0], kaamera.get_px_width(), kaamera.get_px_height())
                else:
                    nurk = abi.kaugus(varav[0], kaamera.get_px_width(), kaamera.get_px_height())[1]
                    mootor.keera_nurgaga(nurk, palli_otsimise_kiirus + 30)

            elif oma_varav and oma_v_kaugus == 0:
                # v2rav on pildi keskel, oleme 5iges suunas l88gi jaoks
                if abi.keskel(oma_varav[0], kaamera.get_px_width(), kaamera.get_px_height(), 10):
                    oma_v_kaugus, nurk = abi.kaugus(oma_varav[0], kaamera.get_px_width(), kaamera.get_px_height())
                else:
                    nurk = abi.kaugus(oma_varav[0], kaamera.get_px_width(), kaamera.get_px_height())[1]
                    mootor.keera_nurgaga(nurk, palli_otsimise_kiirus + 30)

            else:
                # mootor.keera(suund)
                #mootor.keera_ymber_dribbleri(palli_otsimise_kiirus)
                keera_kiirus = abi.noksu_f(loogika1_counter, noksu_periood,
                                           noksu_tasa, palli_otsimise_kiirus, max_otsi_lisa)
                mootor.keera_konst_paremale(keera_kiirus)

        elif loogika == 1:  # otsime palli
            # kui oleme liiga pikalt loogika1's, loome - v5ib olla, et pall dribbleris aga ei tea
            if timeout_counter == 0:
                timeout_counter = time.time()
            else:
                # peaks otsima kaugema v2rava, minema sinnapoole
                if time.time() - timeout_counter > loogika1_timout:
                    loogika = 0  # uus loogika, leiab kaugema v2rava, l2heb selle poole
                    v_kaugus, oma_v_kaugus = 0, 0
                    timeout_counter = 0

            pallid = kaamera.pallid()  # leian pildilt pallid, mis pole v2ljakult v2ljas
            palle = len(pallid)

            if palle == 0:  # leidsin 0 palli
                # arvutame uue kiiruse, millega edasi keerata
                keera_kiirus = abi.noksu_f(loogika1_counter, noksu_periood,
                                           noksu_tasa, palli_otsimise_kiirus, max_otsi_lisa)
                mootor.keera_konst_paremale(keera_kiirus)
                loogika1_counter += 1
            else:
                #print "leidsin palli, mis valjakul"
                loogika = 2  # l2hme j2rgmisesse olekusse
                timeout_counter = 0
                loogika1_counter = 0
                seisund_counter = 0

        elif loogika == 2:  # oleme valinud palli (asub pallid[0]) ja liigume sellele
            seisund_counter += 1

            pallid = kaamera.pallid()
            # kas pall kohe dribleris (l2hedal v2hemasti)
            if len(pallid) > 0 and abi.suus(pallid[0], kaamera.get_px_width(), kaamera.get_px_height()):
                loogika = 3  # l2hme j2rgmisesse olekusse
                print("pall suus")
                mootor.otse(suus_kiirus)
            # kaamera platsi jooni kaugelt ei n2e, kontrollime kas pall t6esti v2ljakul
            elif len(pallid) > 0 and kaamera.sobib(pallid[0]):
                # vaatame kas pall keskel
                keskel = abi.keskel(pallid[0], kaamera.get_px_width(), kaamera.get_px_height(), 0)
                if keskel:
                    print "pall keskel, otse"
                    kaugus, nurk = abi.kaugus(pallid[0], kaamera.get_px_width(), kaamera.get_px_height())
                    otse_kiirus = min(abi.kiirus(kaugus, nurk, otse_liikumise_kiirus), 90 + 3 * seisund_counter)
                    mootor.otse(otse_kiirus)  # liigume otse pallile
                else:  # keerame palli poole,
                    print "pole keskel, soidan palli poole vindiga"
                    kaugus, nurk = abi.kaugus(pallid[0], kaamera.get_px_width(), kaamera.get_px_height())
                    # print("kaugus> " + str(kaugus) + " nurk>nurk " + str(nurk))
                    otse_kiirus = min(abi.kiirus(kaugus, nurk, otse_liikumise_kiirus), 90 + 3 * seisund_counter)
                    keera_kiirus = abi.keera(nurk, keeramise_kiirus)
                    mootor.soida(nurk, otse_kiirus, keera_kiirus)
            else:
                loogika = 1  # pall ei ole v2ljakul, valime uue; l2hme vastavasse olekusse
                print "kaotasin palli"

        elif loogika == 3:  # valitud pall on kohe dribleris
            coilgun.dribbler_go()

            if timeout_counter == 0:
                timeout_counter = time.clock()

            if (time.clock() - timeout_counter) > kadumiseAeg:  # pall on kadunud
                loogika = 1  # loogika tagasi palli otsimisele
                timeout_counter = 0  # aeg ka nulli
                print "pall kadus"
            else:
                mootor.otse(suus_kiirus)  # muidu paneme otse edasi

        elif loogika == 35:  # pall on dribbleris
            # kontrollime, kas oleme 22rejoontest piisavalt kaugel
            if timeout_counter == 0:
                timeout_counter = time.time()
            else:
                if time.time() - timeout_counter > loogika35_timeout:
                    loogika = 4
                    timeout_counter = 0

            if kaamera.get_joon_y() > joone_max_y:  # joon meile liiga l2hedal, tagurdame
                mootor.otse(-40)
            else:
                loogika = 4
                timeout_counter = 0

        elif loogika == 4:  # pall on dribleris ja piisavalt kaugel joontest
            coilgun.dribbler_go()
            if timeout_counter == 0:
                timeout_counter = time.time()
            else:
                if time.time() - timeout_counter > loogika4_timout:
                    loogika = 5
                    timeout_counter = 0

            if not abi.pall_dribleris(yhendus[1]):  # pall on v2rava otsimise ajal tegelikult driblerist kadunud
                loogika = 1  # tagasi palli otsima
                timeout_counter = 0
                print "pall kadus triblerist!"
            else:
                varav = kaamera.varav()
                if len(varav) == 0:  # pole veel v2ravat pildis, keerame ringi
                    if kaamera.viimati_nahtud_varav == 'oma':
                        if kaamera.viimati_nahtud_v_x > kaamera.get_px_width() // 2:
                            # mootor.keera_konst_paremale(palliga_keeramise_kiirus)
                            mootor.keera_konst_paremale_dr(palliga_keeramise_kiirus)
                        else:
                            # mootor.keera_konst_vasakule(palliga_keeramise_kiirus)
                            mootor.keera_konst_vasakule_dr(palliga_keeramise_kiirus)
                    else:
                        if kaamera.viimati_nahtud_v_x > kaamera.get_px_width() // 2:
                            # mootor.keera_konst_vasakule(palliga_keeramise_kiirus)
                            mootor.keera_konst_vasakule_dr(palliga_keeramise_kiirus)
                        else:
                            # mootor.keera_konst_paremale(palliga_keeramise_kiirus)
                            mootor.keera_konst_paremale_dr(palliga_keeramise_kiirus)
                else:
                    # print "leidsin varava"
                    # v2rav on pildi keskel, oleme 5iges suunas l88gi jaoks
                    if abi.keskel(varav[0], kaamera.get_px_width(), kaamera.get_px_height(), 7):
                        algne_kaugus, nurk = abi.kaugus(varav[0], kaamera.get_px_width(), kaamera.get_px_height())
                        loogika = 45
                        timeout_counter = 0
                        print "v2rav keskel"
                    else:  # varav pildis aga mitte keskel, kysime palju keerama peame, vajadusel aeglustame keeramist
                        nurk = abi.kaugus(varav[0], kaamera.get_px_width(), kaamera.get_px_height())[1]
                        keera_kiirus = abs(abi.keera_palliga(nurk, palliga_keeramise_kiirus))
                        # mootor.keera_nurgaga(nurk, keera_kiirus)
                        mootor.keera_ymber_dribbleri_nurgaga(nurk, keera_kiirus)

        elif loogika == 45:  # v2rav keskel, kontrollime, kas midagi ees
            if timeout_counter == 0:
                timeout_counter = time.time()
            else:
                if time.time() - timeout_counter > loogika45_timout:
                    loogika = 5
                    timeout_counter = 0

            p1 = kaamera.pall_paremal_joonel()
            v1 = kaamera.pall_vasakul_joonel()

            if p1 and v1:
                print "vasakul ja paremal joonel"
                mootor.vasakule_varava_poole()
            elif p1:
                print("paremal joonel")
                mootor.vasakule_varava_poole()
            elif v1:
                print("vasakul joonel")
                mootor.paremale_varava_poole()
            else:
                loogika = 5
                timeout_counter = 0

        elif loogika == 5:  # l88

            if timeout_counter == 0:
                timeout_counter = time.time()
                mootor.otse(0)
            elif time.time() - timeout_counter < loogika5_aeg_enne_looki:
                mootor.otse(0)
            elif time.time() - timeout_counter >= loogika5_aeg_enne_looki:
                coilgun.dribbler_stop()  # oleme peatunud, loome
                coilgun.kick()
                print("look tehtud!")
                timeout_counter = time.time()

                # ootame peale looki, uuendame pilti
                while abi.pall_dribleris(yhendus[1]):
                    kaamera.set_frame()

                print("Aeg peale l55gi k2sku, kui pall dribbleris: {0}".format(time.time() - timeout_counter))
                loogika = 1
                timeout_counter = 0

        else:  # siia ei tohiks kukkuda
            loogika = 1
            print "Jaaaaama"

finally:
    coilgun.dribbler_stop()
    del kaamera
    del coilgun
    del mootor

from math import cos, radians, degrees
import abi
import serial
import subprocess


class Liikumine:

    def __init__(self, yhendused):
        self.keeramine = 30
        self.s_vasak = yhendused[0]
        self.s_parem = yhendused[1]
        #self.s_taga = yhendused[2]
        self.s_vasak.write('dr1\n')
        self.s_parem.write('dr1\n')
        #self.s_taga.write('dr0\n')
        self.k_vasak = 0
        self.k_parem = 0
        #self.k_taga = 0
        self.viga = False

    def __del__(self):
        try:
            self.otse(0)
            self.s_vasak.close()
            self.s_parem.close()
            #self.s_taga.close()
        except Exception:
            print "Ei saanud yhendusi kinni"

    def soida(self, suund, kiirus, keere):
        self.liiguta(self.arvuta(suund, kiirus, keere))

    def arvuta(self, suund, kiirus, keere):
        #print("info: ", suund, kiirus, keere)
        vasak = int(round(kiirus * (cos(radians(150 - degrees(suund)))) + keere))
        parem = int(round(kiirus * (cos(radians(30 - degrees(suund)))) + keere))
        taga = int(round(kiirus * (cos(radians(270 - degrees(suund)))) + keere))
        #print("kiirused: ", vasak, parem, taga)
        #print("viga: ", self.viga)
        return [vasak, parem, taga]

    def liiguta(self, kiirused):  # kirjutab rataste mootorite sisesndisse kiirused
        print(subprocess.check_output("ls /dev/ttyACM*", shell=True))
        try:
            self.s_vasak.write('sd' + str(kiirused[0]) + '\n')
            #print "vasak ok"
            self.k_vasak = kiirused[0]
            self.s_parem.write('sd' + str(kiirused[1]) + '\n')
            #print "parem ok"
            self.k_parem = kiirused[1]
            #self.s_taga.write('sd' + str(kiirused[2]) + '\n')
            #self.k_taga = kiirused[2]
        except Exception:
            print ("viga liigutamisel")

    def otse(self, kiirus):
        self.soida(0, kiirus, 0)

    def keera(self, nurk):  # keerab nurgaga m2rgitud suuna poole etteantud kiirusega
        kiirus = abi.keera(nurk, self.keeramine)
        self.soida(0, 0, kiirus)

    def delta(self):
        r_vasak = 0
        r_parem = 0
        #r_taga = 0
        d_vasak = 0
        d_parem = 0
        d_taga = 0
        try:
            self.s_vasak.flush()
            self.s_vasak.write('s\n')
            vasakpre = self.s_vasak.read(6)
            r_vasak = int(vasakpre.split(':')[1][:-3])
            self.s_parem.flush()
            self.s_parem.write('s\n')
            parempre = self.s_parem.read(6)
            r_parem = int(parempre.split(':')[1][:-3])
            #self.s_taga.flush()
            #self.s_taga.write('s\n')
            #tagapre = self.s_taga.read(6)
            #r_taga = int(tagapre.split(':')[1][:-3])
        except Exception:
            pass
        if not self.k_vasak == 0:
            d_vasak = r_vasak/self.k_vasak
        if not self.k_parem == 0:
            d_parem = r_parem/self.k_parem
        #if not self.k_taga == 0:
            #d_taga = r_taga/self.k_taga
        return [d_vasak, d_parem, d_taga]


    def set_viga(self, viga):
        self.viga = viga

    def set_keeramine(self, kiirus):
        self.keeramine = kiirus

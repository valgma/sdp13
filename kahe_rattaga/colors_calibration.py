import cv2
import numpy as np
import cv
import math
import camera_settings
import subprocess

# loeb pildi, kuvab 6 trackbari, Hue, Sat, Val jaoks
# teises aknas jookseb inRange funktsioon, mis vastavalt trackbari vaartustele varvid pildi
#osad mustaks/valgeks
#loeb/salvestab colors_hsv_fixed.txt failist
MORPH_KERNEL_SIZE = 3  # dilate-erode jaoks kerneli suurus
MIN_OBJECT_SIZE = 10  # mitu pixlit peab objekt olema vahemasti, et tuvastada


# A dummy function that is - for trackbars
def nothing(*arg):
    pass


def seadista_camera(cap):
    if cap.isOpened():
        cap.set(cv.CV_CAP_PROP_FRAME_WIDTH, 640)
        cap.set(cv.CV_CAP_PROP_FRAME_HEIGHT, 480)
        cap.set(cv.CV_CAP_PROP_FPS, 30)

        try:
            camera_settings.set_cam_settings_from_cli()
        except subprocess.CalledProcessError as e:
            print("Ei saanud kaamera satteid CLI kaudu korda.")
            print(e.output)


def loe_hsv_fail(f_name):
    with open(f_name, 'r') as f:
        lines = f.read().splitlines()
        pall_hsv = map(int, lines[0].split()[1:4])
        pall_dev = map(int, lines[0].split()[4:7])
        v1_hsv = map(int, lines[1].split()[1:4])
        v1_dev = map(int, lines[1].split()[4:7])
        v2_hsv = map(int, lines[2].split()[1:4])
        v2_dev = map(int, lines[2].split()[4:7])
    return pall_hsv, pall_dev, v1_hsv, v1_dev, v2_hsv, v2_dev


def kirjuta_hsv_fail(f_name, pall_hsv, pall_dev, v1_hsv, v1_dev, v2_hsv, v2_dev):
    with open(f_name, 'w') as f:
        f.write('pall ' + str(pall_hsv + pall_dev).strip('[]').replace(',', '') + '\n')
        f.write('kollane ' + str(v1_hsv + v1_dev).strip('[]').replace(',', '') + '\n')
        f.write('sinine ' + str(v2_hsv + v2_dev).strip('[]').replace(',', ''))


def on_mouse(event, x, y, flag, param):
    if event == cv.CV_EVENT_LBUTTONDOWN:  # hiirenupu vajutamisel loetakse varvid ning muudetakse trackbaride asukohti
        s = img_hsv[y, x]
        cv2.setTrackbarPos('Hue', 'Trackbars', s[0])
        cv2.setTrackbarPos('Sat', 'Trackbars', s[1])
        cv2.setTrackbarPos('Val', 'Trackbars', s[2])


def get_nelinurk(img, BGR, BGR_dev):  # tagastab varvidega maaratud ala ymbritseva min nelinurga keskpunkti
    BGR_array = np.array(BGR)
    BGR_dev_array = np.array(BGR_dev)

    lowerb = BGR_array - BGR_dev_array
    upperb = BGR_array + BGR_dev_array

    dst = np.zeros(img.shape[:2], np.uint8)  # tulemuse hoidmiseks
    cv2.inRange(img, lowerb, upperb, dst)  # tekitab mustvalge pildi (dst) sobivatest punktidest

    kernel = np.ones((MORPH_KERNEL_SIZE, MORPH_KERNEL_SIZE), np.uint8)
    dst = cv2.morphologyEx(dst, cv2.MORPH_CLOSE, kernel)  # dilate + erode

    contours, hier = cv2.findContours(dst, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)  # leiame kujundid
    contours.sort(key=cv2.contourArea, reverse=True)  # igaks juhuks sorteerime suuruse jargi, kasutame suurimat
    if contours and cv2.contourArea(contours[0]) > MIN_OBJECT_SIZE:  # kui yhtegi kujundit ei leitud, tagastame -1, -1
        #(kesk_veerg, kesk_rida), (laius, pikkus), nurk = cv2.minAreaRect(contours[0])
        #return int(kesk_rida), int(kesk_veerg), int(laius), int(pikkus)
        return cv2.minAreaRect(contours[0])
    else:
        #return -1, -1, -1, -1
        return None


def get_kaugus(obj_veerg, obj_rida, px_veerg, px_rida, cam_h, cam_l, alpha):
    '''Tagastab objekti kauguse meetrites roboti esiotsast.
    obj_veer, obj_rida - punkti koordinaadid
    px_veerg, px_rida - pildi mootmed pixlites, resolutsioon
    cam_h - kaamera korgus meetrites
    cam_l - kaamera kaugus roboti esiotsast
    alpha - nurk normaali ning kaamera pildi alumise aare vahe, surnud ala nurk'''

    obj_veerg = float(obj_veerg)
    obj_rida = float(obj_rida)
    fov_v = math.radians(46)  # kaamera vertikaalne vaatevali
    fov_h = math.radians(61)  # horisontaalne vaatevali
    fii = fov_v * (1 - obj_rida / px_rida)  # nurk pildi alumise aare ning punkti vahel
    deeta = fov_h * (obj_veerg / px_veerg - 0.5)  # nurk vertikaalse keskjoone ning punkti vahel, vasakul negatiivne
    dx = cam_h * math.tan(alpha + fii) - cam_l  # kaugus otsesuunas
    kaugus = dx / math.cos(deeta)  # kaugus punktini

    return kaugus, deeta  # tagastame kauguse ning nurga palju poorama peame


def set_tracks(hsv, dev):
    cv2.setTrackbarPos('Hue', 'Trackbars', hsv[0])
    cv2.setTrackbarPos('Sat', 'Trackbars', hsv[1])
    cv2.setTrackbarPos('Val', 'Trackbars', hsv[2])
    cv2.setTrackbarPos('DevH', 'Trackbars', dev[0])
    cv2.setTrackbarPos('DevS', 'Trackbars', dev[1])
    cv2.setTrackbarPos('DevV', 'Trackbars', dev[2])


text_color = [255, 255, 255]
rec_color = [255, 255, 255]

cap = cv2.VideoCapture(0)  # kaamera init, annab natuke veateateid
#cap = cv2.VideoCapture('video1.mkv')
seadista_camera(cap)

cv2.namedWindow("Trackbars")
ret, img = cap.read()  # votame pildi, img1

dst = np.zeros(img.shape[:2], np.uint8)  # mustvalge tulemuse hoidmiseks

cv2.createTrackbar('Hue', 'Trackbars', 0, 179, nothing)
cv2.createTrackbar('Sat', 'Trackbars', 0, 255, nothing)
cv2.createTrackbar('Val', 'Trackbars', 0, 255, nothing)
cv2.createTrackbar('DevH', 'Trackbars', 0, 100, nothing)
cv2.createTrackbar('DevS', 'Trackbars', 0, 100, nothing)
cv2.createTrackbar('DevV', 'Trackbars', 0, 100, nothing)
cv2.createTrackbar('Target', 'Trackbars', 0, 2, nothing)

try:
    pall_hsv, pall_dev, v1_hsv, v1_dev, v2_hsv, v2_dev = loe_hsv_fail('colors_hsv_fixed.txt')
except:
    pall_hsv, pall_dev, v1_hsv, v1_dev, v2_hsv, v2_dev = [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0,
                                                                                                                 0]

cv2.setTrackbarPos('Target', 'Trackbars', 0)  # esmalt kuvame palli
eelmine_target = 0
hsv, dev = pall_hsv, pall_dev

#algpositsioonid
set_tracks(hsv, dev)
camera_settings.set_cam_settings_from_cli()

while True:
    ret, img = cap.read()
    img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    hsv[0] = cv2.getTrackbarPos('Hue', 'Trackbars')  # Get the new value from Trackbar
    hsv[1] = cv2.getTrackbarPos('Sat', 'Trackbars')
    hsv[2] = cv2.getTrackbarPos('Val', 'Trackbars')
    dev[0] = cv2.getTrackbarPos('DevH', 'Trackbars')
    dev[1] = cv2.getTrackbarPos('DevS', 'Trackbars')
    dev[2] = cv2.getTrackbarPos('DevV', 'Trackbars')
    target = cv2.getTrackbarPos('Target', 'Trackbars')

    #vastavalt targetile kirjutame yle vaartused
    if target == eelmine_target:  # kui me ei ole muutnud targeti vaartus
        if target == 0:
            pall_hsv, pall_dev = hsv, dev
        elif target == 1:
            v1_hsv, v1_dev = hsv, dev
        elif target == 2:
            v2_hsv, v2_dev = hsv, dev
        else:
            print "jama targetiga"
    else:  # kui me oleme muutnud targeti vaartus, tuleb lugeda sisse uued ning trackbarid uuendada
        if target == 0:
            hsv, dev = pall_hsv, pall_dev
        elif target == 1:
            hsv, dev = v1_hsv, v1_dev
        elif target == 2:
            hsv, dev = v2_hsv, v2_dev
        else:
            print "jama targetiga"
        set_tracks(hsv, dev)

    eelmine_target = target

    HSV_array = np.array(hsv)
    HSV_dev_array = np.array(dev)

    lowerb = HSV_array - HSV_dev_array
    upperb = HSV_array + HSV_dev_array

    cv2.inRange(img_hsv, lowerb, upperb, dst)  # tekitab mustvalge pildi (dst) sobivatest punktidest

    kernel = np.ones((MORPH_KERNEL_SIZE, MORPH_KERNEL_SIZE), np.uint8)
    dst = cv2.morphologyEx(dst, cv2.MORPH_CLOSE, kernel)  # dilate + erode

    #obj_rida, obj_veerg, laius, pikkus = getNelinurkAlumineKeskpunkt(img_hsv, hsv, dev)   #leiame objekti keskpunkti
    min_rec = get_nelinurk(img_hsv, hsv, dev)

    if min_rec:  # leidsime objekti yles, siis anname infot
        #cv2.putText(img, "Obj loc: " + str(obj_veerg) + "," + str(obj_rida), (50, 60), cv2.FONT_HERSHEY_SIMPLEX, 1, text_color) #kuvame obj asukoha
        obj_veerg, obj_rida = int(min_rec[0][0]), int(min_rec[0][1])  # keskpunkt
        kaugus, deeta = get_kaugus(obj_veerg, obj_rida, 640, 480, 0.3, 0.3, math.radians(45))
        cv2.putText(img, "Obj loc: " + str(obj_veerg) + ", " + str(obj_rida), (50, 30), cv2.FONT_HERSHEY_SIMPLEX, 1,
                    text_color)  # kuvame obj asukoha
        cv2.putText(img, "kaugus: " + str(round(kaugus, 2)), (50, 60), cv2.FONT_HERSHEY_SIMPLEX, 1,
                    text_color)  # kuvame kauguse punktini
        #cv2.rectangle(img, pt1, pt2, rec_color)
        box = cv2.cv.BoxPoints(min_rec)
        box = np.int0(box)
        cv2.drawContours(img, [box], 0, rec_color, 2)

        #esitluse jaoks, keerame vasakule voi paremale
        if abs(deeta) < math.radians(2.5):
            cv2.putText(img, "mine otse", (50, 90), cv2.FONT_HERSHEY_SIMPLEX, 1, text_color)
        elif deeta < 0:
            cv2.putText(img, "keera vasakule {0} kraadi".format(round(math.degrees(abs(deeta)), 1)), (50, 90),
                        cv2.FONT_HERSHEY_SIMPLEX, 1, text_color)
        else:
            cv2.putText(img, "keera paremale {0} kraadi".format(round(math.degrees(abs(deeta)), 1)), (50, 90),
                        cv2.FONT_HERSHEY_SIMPLEX, 1, text_color)

    else:  # ei leidnud palli, esitluse jaoks
        cv2.putText(img, "Ei leidnud objekti. Keera!", (50, 90), cv2.FONT_HERSHEY_SIMPLEX, 1, text_color)

    #esitluse jaoks, joon keskele
    cv2.line(img, (320, 0), (320, 479), rec_color)

    #mustvalge pildi kuvamiseks (testimisel)
    cv2.imshow("mustvalge", dst)

    cv2.setMouseCallback("s - save, q - valju", on_mouse, 0)

    cv2.imshow("s - save, q - valju", img)

    ch = cv2.waitKey(5)
    if ch == 1048689 or ch == 1048603 or ch == ord('q'):  # 'q' or 'Esc'
        break
    elif ch == 1048691 or ch == ord('s'):  # 's'
        kirjuta_hsv_fail('colors_hsv_fixed.txt', pall_hsv, pall_dev, v1_hsv, v1_dev, v2_hsv, v2_dev)

cap.release()
cv2.destroyAllWindows()

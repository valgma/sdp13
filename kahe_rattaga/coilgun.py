import serial
import time


class Coilgun:
    KICK_TIME = 15000  # m22rab, kui pika l88gi teeme
    COIL_ID = 4
    MIN_TIME_BETWEEN_KICKS = 2

    def __init__(self, port):  # v5i on vaja kasutada yhendused[?]
        if port.__class__.__name__ == 'Serial':
            self.port = port
            self.last_kick_time = time.time()
        else:
            raise TypeError('Coilguni yhendus polnud Serial')

        try:
            self.init_coilgun()
        except Exception as e:
            print(e.message)
            print('Mingi jama coilguni seadete panemisel')

    def __del__(self):
        try:
            self.port.flush()
            # self.port.write('ac0\n')  # automaatne laadimine v2lja
            self.lae_tyhjaks()
            self.port.close()
        except Exception as e:
            print(e.message)
            print("Ei saanud coilguni yhendust kinni. ETTEVAATUST, LAE ISE TYHJAKS!")

    def init_coilgun(self):
        self.port.flush()

        # if self.get_id() != Coilgun.COIL_ID:
        #     raise ValueError('Vale coili ID: ' + str(self.get_id()))

        self.port.timeout = 0.5   # read k2su timeout
        # self.port.write('ac1\n')  # automaatne laadimine sisse
        self.port.write('fs0\n')  # automaatne tyhjakslaadimine sisse, 1.6s peale viimase ping k2su saamist
        self.laadimine_sisse()

    def laadimine_sisse(self):
        self.port.write('c\n')

    def laadimine_loomine_valja(self):
        self.port.write('e\n')

    def lae_tyhjaks(self):
        self.port.write('fs1\n')
        vastus = ""
        while vastus != "discharged":  # aadame niikaua k2sku, kuni saame vastuse et on tyhi
            print("Yritan coilguni tyhjaks laadida.")
            self.port.write('d\n')
            vastus = self.port.read(10).strip()  # TODO kas 10 on sobiv baitide arv lugemiseks?

        print("Coilgun tyhi!")

    def ping(self):
        self.port.write('p\n')

    def kick(self):
        try:
            if time.time() - self.last_kick_time > Coilgun.MIN_TIME_BETWEEN_KICKS:
                print "yritan lyya"
                self.port.write('k' + str(Coilgun.KICK_TIME) + '\n')
                print "saatsin k2su ara"
                self.last_kick_time = time.time()
                self.laadimine_sisse()
            else:
                print "tahtsin lyya, aga eelmine look oli liiga hiljuti"
        except Exception:
            print ("Viga l88misel.")

    def get_id(self):
        id_str = self.port.read(7).split(':')[1][:-2]
        return int(id_str)

import cv2
import cv
import numpy as np
import abi
import camera_settings
import subprocess


class Pilt:
    BALL_MORPH_KERNEL_SIZE = 3  # dilate-erode jaoks
    MIN_BALL_SIZE = 10
    MIN_GOAL_SIZE = 100

    def __init__(self, cam_id, varav_id):
        self.cap = cv2.VideoCapture(cam_id)
        self.frame = None  # yhe frame masiiv
        self.px_width = 640
        self.px_height = 480
        self.seadista_camera()
        self.pall_hsv = [0, 0, 0]
        self.pall_dev = [0, 0, 0]
        self.varav_hsv = [0, 0, 0]
        self.varav_dev = [0, 0, 0]
        self.set_pall_hsv()
        self.set_varav_hsv(varav_id)

    def __del__(self):
        cv2.destroyAllWindows()
        self.cap.release()

    def seadista_camera(self):
        if self.cap.isOpened():
            self.cap.set(cv.CV_CAP_PROP_FRAME_WIDTH, self.px_width)
            self.cap.set(cv.CV_CAP_PROP_FRAME_HEIGHT, self.px_height)
            self.cap.set(cv.CV_CAP_PROP_FPS, 30)

            # try:  # seaded, mida opencv ei lase, auto whitebalance off!
            #     camera_settings.set_cam_settings_from_cli()
            # except subprocess.CalledProcessError as e:
            #     print("Jama kaamera seadistamisega CLI kaudu.")
            #     print(e.output)

    def set_pall_hsv(self):  # loeb failist palli varvid
        with open('colors_hsv_fixed.txt', 'r') as f:
            lines = f.read().splitlines()
        self.pall_hsv = map(int, lines[0].split()[1:4])
        self.pall_dev = map(int, lines[0].split()[4:7])

    def set_varav_hsv(self, varav_id):  # loeb failist varava varvid, id = 0 (kollane) voi 1 (sinine)
        with open('colors_hsv_fixed.txt', 'r') as f:
            _lines = f.read().splitlines()
        self.varav_hsv = map(int, _lines[varav_id + 1].split()[1:4])
        self.varav_dev = map(int, _lines[varav_id + 1].split()[4:7])

    def set_frame(self):  # loeb kaamerast pildi ja teisendab hsv'ks
        if self.cap.isOpened:
            img = self.cap.read()[1]
            self.frame = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    def get_frame(self):
        # self.set_frame()
        return self.frame

    def get_px_width(self):
        return self.px_width

    def get_px_height(self):
        return self.px_height

    @staticmethod
    def get_contours_frame(img, hsv, dev):  # tagastab leitud kujundid, sorteeritud suurima jargi
        hsv_array = np.array(hsv)
        hsv_dev_array = np.array(dev)

        lowerb = hsv_array - hsv_dev_array
        upperb = hsv_array + hsv_dev_array

        dst = np.zeros(img.shape[:2], np.uint8)  # tulemuse hoidmiseks
        cv2.inRange(img, lowerb, upperb, dst)  # tekitab mustvalge pildi (dst) sobivatest punktidest

        kernel = np.ones((Pilt.BALL_MORPH_KERNEL_SIZE, Pilt.BALL_MORPH_KERNEL_SIZE), np.uint8)
        dst = cv2.morphologyEx(dst, cv2.MORPH_CLOSE, kernel)  # dilate + erode

        contours, hier = cv2.findContours(dst, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)  # leiame kujundid
        contours.sort(key=cv2.contourArea, reverse=True)

        return contours

    def pallid(self):  # peab tagastame pallide jarjendi, sobivaim eesmpool
        # TODO pallid() - hetkel tagastab ainult suurima palli...praegu OK
        pallid = []
        self.set_frame()  # votame pildi

        # uus jupp, leiab varava ning paneb mustaks selle bloki
        # goal = self.varav()
        # if goal:
        #     x, y, dx, dy = goal
        #     self.frame[x:(x + dx), y:(y + dy)] = np.zeros((dx, dy), np.uint8)

        contours = Pilt.get_contours_frame(self.frame, self.pall_hsv, self.pall_dev)

        if contours and cv2.contourArea(contours[0]) >= Pilt.MIN_BALL_SIZE:
            pallid.append(cv2.boundingRect(contours[0]))

        return pallid

    def sobib(self, pall):  # kas pall on valjakul (ei ole piiridest valjas)
        # TODO sobib() on vaga kahtlane hetkel???
        return True
        # esmalt yritab leida joont, kui leiab, siis kontrollib, et pall oleks allpool joont
        if not pall:
            raise Exception('palli pole')
        else:
            # treshime, et jaaks ainult vaga mustad alad
            gray = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
            ret, dst = cv2.threshold(gray, 50, 255, cv2.THRESH_BINARY_INV)
            kernel = np.ones((5, 5), np.uint8)
            dst = cv2.morphologyEx(dst, cv2.MORPH_OPEN, kernel)  # erode + dilate, myra eemaldamiseks

            # leiame jooned
            min_line_length = 150
            max_line_gap = 10
            lines = cv2.HoughLinesP(dst, 1, np.pi / 180, 100, min_line_length, max_line_gap)

            # palli koord
            pall_x = pall[0] + pall[2] / 2
            pall_y = pall[1] + pall[3] / 2

            # kaime koik leitud jooned labi ning vaatame, kas pall on joonest allpool,
            # kui leiame mone, kus pole, tagastame False
            for x1, y1, x2, y2 in lines[0]:
                if not abi.kas_pall_joonest_allpool(pall_x, pall_y, x1, y1, x2, y2, self.px_width):
                    return False

        return True

    def leia(self, pall, liikusin):  # leiab etteantud palli jargi uuesti pildilt palli...
        # TODO implement leia() - hetkel pole vaja, kasutame suurimat palli
        return pall

    def varav(self):  # leiab pildilt varava, tagastab
        # TODO varav() - hetkel naiivne, leiab lihtsalt boundingRecti ymber suurima kontuuri
        varav = []
        self.set_frame()  # votame pildi

        contours = self.get_contours_frame(self.frame, self.varav_hsv, self.varav_dev)

        if contours and cv2.contourArea(contours[0]) >= Pilt.MIN_GOAL_SIZE:
            varav.append(cv2.boundingRect(contours[0]))
        print varav
        return varav


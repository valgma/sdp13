# juhtkood
from liikumine import Liikumine  # mootorite juhtimine
from pilt import Pilt  # kaamerast pilt ja asjad
from coilgun import Coilgun
import abi  # abi funktsioonid arvutamiseks
import time
import cv2
import camera_settings
import random
import subprocess

# seaded
suund = 21  # default otsimise suund(+/-) ja kiirus
kiirus = 80  # otse liikumise kiirus
keere = 21
kadumiseAeg = 3.0  # aeg mille jooksul pildist kadunud pall, mis ei ole j6udnud driblerisse, lugeda kadunuks
suus_kiirus = 15
suus_sleep_time = 0.5
loogika1_kick_time = 10
loogika4_timout = 10

goal_id = 0  # 0 - kollane, 1 - sinine

#mootorite id-d
vasak = 1
parem = 2
taga = 3
juhuslik = 100

#coilguni id
coil = 4

#muutujad
aeg = 0
loogika1_aeg = 0
loogika4_aeg = 0
mine_otse = False  # kui loogika1'st loogika4, siis peaks minema v2ravale lahemale

#loogika: 1 - otsin palli; 2 - liigun pallile; 3 - pall kohe dribleris aga pildilt kohe v2ljas; 4 - pall dribleri; 5 - l88n
#loogika TODO: 0 - nuppude asjad
loogika = 1
loogika_abi = 0  # printimisel abiks, prindime ainult muutused

#kaamera ja mootorite init
yhendus = abi.yhendused([vasak, parem, coil])
mootorid = yhendus[0:2]
s_coil = yhendus[2]

kaamera = Pilt(0, goal_id)
mootor = Liikumine(mootorid)
coilgun = Coilgun(s_coil)

#sea default mootori keeramise suund ja kiirus
mootor.set_keeramine(keere)

# lisame trackbari v2rava valimiseks
def on_trackbar(tb_value):
    global goal_id
    try:
        goal_id = tb_value
        kaamera.set_varav_hsv(goal_id)
        print 'varava ID:', goal_id
    except:
        print 'Viga varava ID muutmisega'


def on_trackbar_2(tb_value):
    camera_settings.set_cam_settings_from_cli()


switch = '0 : Kollane\n1 : Sinine'
switch1 = 'Trigger cam settings'
cv2.namedWindow('Kaamera pilt')
cv2.createTrackbar(switch, 'Kaamera pilt', goal_id, 1, on_trackbar)
_tb1 = 0
cv2.createTrackbar(switch1, 'Kaamera pilt', _tb1, 1, on_trackbar_2)
# trackbari osa lopeb

try:
    while abi.start():
        try:  # kuvab pilti, mida kaamera naeb. v2ike overhead on pildi konvertimisel hetkel
            bgr_img = cv2.cvtColor(kaamera.get_frame(), cv2.COLOR_HSV2BGR)
            cv2.putText(bgr_img, "Varava ID: " + str(goal_id), (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 1, [0, 0, 255], 2)
            cv2.putText(bgr_img, "Loogika: " + str(loogika), (10, 90), cv2.FONT_HERSHEY_SIMPLEX, 1, [0, 0, 255], 2)
            cv2.imshow('Kaamera pilt', bgr_img)
            ch = cv2.waitKey(5)
            if ch == 1048689 or ch == 1048603 or ch == ord('q'):  # 'q' or 'Esc'
                break
        except:
            print 'probleem pildi naitamisel'

        coilgun.ping()  # pingime, loodetavasti pole probleem, et nii tihti

        #stalli tuvastus
#        try:
#            delta = mootor.delta()
#        except Exception:
#            print ("Viga stalli kysimisel")

        if loogika != loogika_abi:  # prindime muutused v2lja
            print "loogika: ", loogika
            loogika_abi = loogika

        if loogika == 1:  # otsime palli
            # kui oleme liiga pikalt loogika1's, loome - v5ib olla, et pall dribbleris aga ei tea
            if loogika1_aeg == 0:
                loogika1_aeg = time.time()
            else:
                if time.time() - loogika1_aeg > loogika1_kick_time:
                    loogika = 4
                    mine_otse = True
                    loogika1_aeg = 0

            pallid = kaamera.pallid()  # leian pildilt pallid
            palle = len(pallid)

            if palle == 0:  # leidsin 0 palli
                if juhuslik >= 3:
                    juhuslik = random.randint(0, 100)
                    mootor.keera(suund)
                else:
                    juhuslik += 1
                    mootor.otse(0)

            elif palle == 1:  # leidsin 1 palli
                if kaamera.sobib(pallid[0]):  # kui pall v2ljakul
                    loogika = 2  # l2hme j2rgmisesse olekusse
                    loogika1_aeg = 0
                    print "leidsin palli, mis valjakul"
                else:  # pall ei ole v2ljakul
                    mootor.keera(suund)

            else:  # leidsin rohkem palle
                vahe = []
                for i in range(palle):  # hakka sorteeritud palle vaatama
                    if kaamera.sobib(pallid[i]):  # kui vaadatud pall sobib
                        vahe = pallid[i]
                        break  # rohkem pole vaja vaadata

                if len(vahe) == 0:  # yksgi pall ei olnud v2ljakul
                    mootor.keera(suund)

                else:  # pall sobis
                    pallid = []
                    pallid.append(vahe)
                    loogika = 2  # l2hme j2rgmisesse olekusse
                    loogika1_aeg = 0

        elif loogika == 2:  # oleme valinud palli (asub pallid[0]) ja liigume sellele
            #pallid = kaamera.leia(pallid[0], liikusin)  #uuenda palli asukohta
            pallid = kaamera.pallid()
            if len(pallid) > 0 and abi.suus(pallid[0], kaamera.get_px_width(), kaamera.get_px_height()):  # kas pall kohe dribleris (l2hedal v2hemasti)
                loogika = 3  # l2hme j2rgmisesse olekusse
                print("pall suus")
                mootor.otse(suus_kiirus)
                time.sleep(suus_sleep_time)
            elif len(pallid) > 0 and kaamera.sobib(pallid[0]):  # kaamera platsi jooni kaugelt ei n2e, kontrollime kas pall t6esti v2ljakul
                keskel = abi.keskel(pallid[0], kaamera.get_px_width(), kaamera.get_px_height())  # vaatame kas pall keskel
                if keskel:
                    print "pall keskel, otse"
                    mootor.otse(kiirus)  # liigume otse pallile
                else:  # keerame palli poole,
                    print "pole keskel, keeran palli poole"
                    kaugus, nurk = abi.kaugus(pallid[0], kaamera.get_px_width(), kaamera.get_px_height())
                    # print("kaugus> " + str(kaugus) + " nurk> " + str(nurk))
                    otse = abi.kiirus(kaugus, nurk, kiirus)
                    # mootor.soida(nurk, otse, abi.keera(nurk, keere))
                    mootor.keera(nurk)  # kahe rattaga siis yritame ainult keerata
            else:
                loogika = 1  # pall ei ole v2ljakul, valime uue; l2hme vastavasse olekusse
                print "kaotasin palli"

        elif loogika == 3:  # valitud pall on kohe dribleris
            if aeg == 0:
                aeg = time.clock()
            if (time.clock() - aeg) > kadumiseAeg:  # pall on kadunud
                loogika = 1  # loogika tagasi palli otsimisele
                aeg = 0  # aeg ka nulli
                print "pall kadus"
            elif abi.pall_dribleris():  # kas sensor n2eb palli
                loogika = 4  # pall on n2htav dribleri sensorist, l2hme j2rgmisesse olekusse
                print "pall on tribleris, otsin v2ravat"
            else:
                mootor.otse(kiirus)  # muidu paneme otse edasi, voib tekkida spamm
                time.sleep(0.02)  # ootame natuke igaks juhuks

        elif loogika == 4:  # pall on dribleris
            if loogika4_aeg == 0:
                loogika4_aeg = time.time()
            else:
                if time.time() - loogika4_aeg > loogika4_timout:
                    loogika = 5
                    loogika4_aeg = 0

            if not abi.pall_dribleris():  # pall on v2rava otsimise ajal tegelikult driblerist kadunud
                loogika = 1  # tagasi palli otsima
                loogika4_aeg = 0
                print "pall kadus triblerist! hetkel ei tohiks juhtuda"
            else:
                varav = kaamera.varav()
                if len(varav) == 0:  # pole veel v2ravat pildis, keerame ringi
                    mootor.keera(suund)
                else:
                    # print "leidsin varava"
                    if abi.keskel_varav(varav[0], kaamera.get_px_width(), kaamera.get_px_height()):  # v2rav on pildi keskel, oleme 5iges suunas l88gi jaoks
                        algne_kaugus, nurk = abi.kaugus(varav[0], kaamera.get_px_width(), kaamera.get_px_height())
                        otse_aeg = 0
                        if mine_otse:
                            print "yritan otse minna"
                            otse_aeg = time.time()
                        while mine_otse:
                            if time.time() - otse_aeg > 10:
                                print "otse timeout"
                                break
                            varav = kaamera.varav()
                            kaugus, nurk = abi.kaugus(varav[0], kaamera.get_px_width(), kaamera.get_px_height())
                            mootor.otse(kiirus)
                            if kaugus < algne_kaugus / 2:
                                algne_kaugus = 0
                                mine_otse = False
                                print "otse pool maad"
                                break

                        loogika = 5
                        loogika4_aeg = 0
                        print "v2rav keskel, loon!"
                    else:  # varav pildis aga mitte keskel, kysime palju keerama peame, vajadusel aeglustame keeramist
                        nurk = abi.kaugus(varav[0], kaamera.get_px_width(), kaamera.get_px_height())[1]
                        mootor.keera(nurk)


        elif loogika == 5:  # l88
            mootor.otse(0)
            time.sleep(0.3)
            coilgun.kick()
            print("look tehtud!")
            loogika = 1

            # # praegu peame siis varava poole porutama, pall juba tribleris
            # varav = kaamera.varav()
            # if len(varav) == 0:  # varav kadunud, mingi jama, tagasi palli otsima
            #     print "varav kadunud"
            #     loogika = 1
            # else:
            #     kaugus, nurk = abi.kaugus(varav[0], kaamera.get_px_width(), kaamera.get_px_height())
            #     if kaugus < 0.1:  # oleme juba otsaga varavas, j22me seisma
            #         print "olen kohal!"
            #         break
            #
            #     if abi.keskel(varav[0], kaamera.get_px_width(), kaamera.get_px_height()):  # v2rav on pildi keskel, oleme 5iges suunas l88gi jaoks
            #         mootor.otse(kiirus)  # paneme varava poole ajama
            #     else:  # varav pildis aga mitte keskel, kysime palju keerama peame, vajadusel aeglustame keeramist
            #         mootor.keera(nurk)

        else:  # siia ei tohiks kukkuda
            loogika = 1

finally:
    del kaamera
    del coilgun
    del mootor

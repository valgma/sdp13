# Kontrolleriga juhtimise kood
from liikumine import Liikumine
import kontroller
from math import atan2, degrees, sqrt
import time
import abi
from coilgun import Coilgun

#mootori kiiruse piirid
mkiirus = 50
mkeere = 50

vektmax = sqrt(2)
ided = [1, 2, 3, 4]  # viimane on coil

yhendus = abi.yhendused(ided)
mootorid = yhendus[0:3]
s_coil = yhendus[3]
mootor = Liikumine(mootorid)
coilgun = Coilgun(s_coil)

while True:
    coilgun.ping()
    sisend = kontroller.seis()
    x = -1.0 * sisend[1]
    y = -1.0 * sisend[0]
    z = sisend[3]
    vekt = sqrt(x * x + y * y)
    nurk = atan2(y, x)
    if abs(vekt) < 0.1:
        kiirus = 0
    else:
        kiirus = vekt * mkiirus
    if abs(z) < 0.1:
        keere = 0
    else:
        keere = mkeere * z
    mootor.soida(nurk, kiirus, keere)
    if sisend[6] == 1:
        print "main: look"
        coilgun.kick()
    time.sleep(0.1)

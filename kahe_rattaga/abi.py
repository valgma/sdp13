import math
import serial
import subprocess


def keskel(kujund, frame_width, frame_height):  # tagastab, kas kujund (pall, varav) pildi keskel voi mitte
    # kujund on siis boundingRect (x_yl_vasak, y_yl_vasak, laius, korgus)
    lotk = 4      # kraadides
    kujund_kaugus, nurk = kaugus(kujund, frame_width, frame_height)

    if abs(nurk) < math.radians(lotk):
        return True
    else:
        return False


def keskel_varav(kujund, frame_width, frame_height):  # tagastab, kas kujund (pall, varav) pildi keskel voi mitte
    # kujund on siis boundingRect (x_yl_vasak, y_yl_vasak, laius, korgus)
    lotk = 10      # kraadides
    kujund_kaugus, nurk = kaugus(kujund, frame_width, frame_height)

    if abs(nurk) < math.radians(lotk):
        return True
    else:
        return False


def get_kaugus(obj_veerg, obj_rida, px_width, px_height, cam_h, cam_l, alpha):
    """ Tagastab objekti kauguse meetrites roboti esiotsast.
    obj_veer, obj_rida - punkti koordinaadid
    px_veerg, px_rida - pildi mootmed pixlites, resolutsioon
    cam_h - kaamera korgus meetrites
    cam_l - kaamera kaugus roboti esiotsast
    alpha - nurk normaali ning kaamera pildi alumise aare vahe, surnud ala nurk
    """

    obj_veerg = float(obj_veerg)
    obj_rida = float(obj_rida)
    fov_v = math.radians(46)  # kaamera vertikaalne vaatevali
    fov_h = math.radians(61)  # horisontaalne vaatevali
    fii = fov_v * (1 - obj_rida / px_height)        # nurk pildi alumise aare ning punkti vahel
    deeta = fov_h * (obj_veerg / px_width - 0.5)    # nurk vertikaalse keskjoone ning punkti vahel, vasakul negatiivne
    dx = cam_h * math.tan(alpha + fii) - cam_l      # kaugus otsesuunas

    try:
        kaugus = dx / math.cos(deeta)                   # kaugus punktini
    except ZeroDivisionError:
        kaugus = 6.5

    return kaugus, deeta  # tagastame kauguse ning nurga palju poorama peame


def kaugus(kujund, px_width, px_height):  # kujundi kaugus, sobiva punkti kujudil leiame ise
    cam_h = 0.3
    cam_l = 0.3
    alpha = math.radians(45)
    obj_veerg = kujund[0] + kujund[2] // 2  # boundingRect keskpunkt
    obj_rida = kujund[1] + kujund[3]   # votame alumise aare hetkel

    return get_kaugus(obj_veerg, obj_rida, px_width, px_height, cam_h, cam_l, alpha)


def start():
    return True


def suus(pall, frame_width, frame_height):  # kontrollib, kas pall on h2sti pildi all (ning keskel)
    # pall siis ikka boundingRect
    pixleid_alt_servast = 40
    if pall:  # kontrollime, kas ikka saime midagi
        # palli ylemine nurk on alumisest servest vahem, kui pixleid_alt_servast
        # kaugus keskpunktist (laiuses) on vaiksem kui viiendik ekraani
        if pall[1] > (frame_height - pixleid_alt_servast) and abs(pall[0] + pall[2] / 2 -
                        frame_width / 2) < (frame_width / 3):
            #TODO abi.suus() - testida sobiv pixlite arv korguse jaoks
            return True
    return False


def kas_pall_joonest_allpool(pall_x, pall_y, x1, y1, x2, y2, frame_width):
    if abs(x1 - x2) > 0.1:  # punktid pole sama vertikaalse joone peal
        sirge_y = y1 + (y2 - y1) / float(x2 - x1) * (pall_x - x1)  # sirge vorrand, y=f(pall_x)
        return pall_y < sirge_y  # palli y-koord < leitud sirge vorrandi y-koord f(pall_x)
    else:  # vordleme, kas joone voi palli x-koord on keskpunktile lahemal, kui palli oma, siis OK
        return abs(x1 - frame_width / 2) > abs(pall_x - frame_width / 2)


def keera(nurk, kiirus):  # nurk - rad; negatiive - vasakule; pos - paremale
    kordaja = 1.0  # vasakule pooramiseks kiirus positiivne

    if nurk > 0:  # paremale kiirus negatiivne
        kordaja = -1.0

    if abs(nurk) < 0.15:  # kui nurk piisavalt vaike, vahendame keeramise kiirust kuni pooleni
        return kordaja * kiirus * (0.5 + abs(nurk) * 3.0)
    else:
        return kordaja * kiirus


def kiirus(kaugus, nurk, maksimum):  # soltuvalt kaugusest objektini ning nurgast, tagastab soidu kiiruse
    magik = abs(nurk / kaugus)
    if magik < 1:
        magik = 1.0
    return int(maksimum / magik)


def pall_dribleris():
    return True


def yhendused(ided):  # kaks mootorit + coil
    vasak = ided[0]
    parem = ided[1]
    #taga = ided[2]
    coil = ided[2]
    olemas = 0
    s_vasak = None
    s_parem = None
    s_taga = None
    s_coil = None

    while olemas < 3:
        try:
            pordidpre = subprocess.check_output("ls /dev/ttyACM*", shell=True)
            pordid = pordidpre.split('\n')
            pordid.pop()
            if not (len(pordid) == 3):
                print "Kolm mootorit v6i coil ei ole taga!"
                print pordid
                pordid = False
        except Exception:
            print "Kolm mootorit v6i coil ei ole taga!"
            pordid = False
        if pordid:
            print pordid
            for pordo in pordid:
                ser = serial.Serial(port=pordo, baudrate=115200)
                print "up"
                ser.flush()
                print "flush"
                ser.write('?\n')
                print "?"
                numberpre = ser.read(7)
                print numberpre
                if numberpre.find("dis") > -1:
                    s_coil = ser
                    olemas += 1
                try:
                    number = int(numberpre.split(':')[1][:-2])
                    if number == vasak:
                        s_vasak = ser
                        olemas += 1
                    elif number == parem:
                        s_parem = ser
                        olemas += 1
                    # elif number == taga:
                    #     s_taga = ser
                    #     olemas += 1
                    else:
                        ser.close()
                except Exception:
                    pass
            if not olemas == 3:
                print "Ei saanud yhendusi!"
                olemas = 0
    return [s_vasak, s_parem, s_coil]

import cv2
import numpy as np
import cv
import math
import camera_settings
import subprocess
from pilt import Pilt
import abi
import pickle

COL_FILE = Pilt.COL_FILE_PICKLE
text_color = [255, 255, 255]
rec_color = [0, 0, 0]


# A dummy function that is - for trackbars
def nothing(*arg):
    pass


def on_mouse(event, x, y, flag, param):
    global hsv
    if event == cv.CV_EVENT_LBUTTONDOWN:  # hiirenupu vajutamisel loetakse varvid ning muudetakse trackbaride asukohti
        s = img_hsv[y, x]
        cv2.setTrackbarPos('Hue', 'Trackbars', s[0])
        cv2.setTrackbarPos('Sat', 'Trackbars', s[1])
        cv2.setTrackbarPos('Val', 'Trackbars', s[2])
        hsv = s


def set_tracks(hsv, dev):
    cv2.setTrackbarPos('Hue', 'Trackbars', hsv[0])
    cv2.setTrackbarPos('Sat', 'Trackbars', hsv[1])
    cv2.setTrackbarPos('Val', 'Trackbars', hsv[2])
    cv2.setTrackbarPos('DevH', 'Trackbars', dev[0])
    cv2.setTrackbarPos('DevS', 'Trackbars', dev[1])
    cv2.setTrackbarPos('DevV', 'Trackbars', dev[2])


def get_tracks():
    hsv[0] = cv2.getTrackbarPos('Hue', 'Trackbars')  # Get the new value from Trackbar
    hsv[1] = cv2.getTrackbarPos('Sat', 'Trackbars')
    hsv[2] = cv2.getTrackbarPos('Val', 'Trackbars')
    dev[0] = cv2.getTrackbarPos('DevH', 'Trackbars')
    dev[1] = cv2.getTrackbarPos('DevS', 'Trackbars')
    dev[2] = cv2.getTrackbarPos('DevV', 'Trackbars')
    target = cv2.getTrackbarPos('Target', 'Trackbars')

    return hsv, dev, target


def get_target(nr):
    m = {0: 'orange', 1: 'yellow', 2: 'blue', 3: 'black', 4: 'white', 5: 'green'}
    return m[nr]


kaamera = Pilt(0, 0)  # varavad, 0-kollane, 1-sinine

cv2.namedWindow("Trackbars")
kaamera.set_frame()
img = kaamera.get_frame_bgr()  # votame pildi, img1

dst = np.zeros(img.shape[:2], np.uint8)  # mustvalge tulemuse hoidmiseks

cv2.createTrackbar('Hue', 'Trackbars', 0, 179, nothing)
cv2.createTrackbar('Sat', 'Trackbars', 0, 255, nothing)
cv2.createTrackbar('Val', 'Trackbars', 0, 255, nothing)
cv2.createTrackbar('DevH', 'Trackbars', 0, 100, nothing)
cv2.createTrackbar('DevS', 'Trackbars', 0, 100, nothing)
cv2.createTrackbar('DevV', 'Trackbars', 0, 100, nothing)
cv2.createTrackbar('Target', 'Trackbars', 0, 5, nothing)


with open(COL_FILE) as f:
    colors_hsv = pickle.load(f)

cv2.setTrackbarPos('Target', 'Trackbars', 0)  # esmalt kuvame palli
eelmine_target = 0
hsv, dev = colors_hsv['orange']

print(hsv, dev)

#algpositsioonid
set_tracks(hsv, dev)
#camera_settings.set_cam_settings_from_cli()

def on_trackbar_2(tb_value):
    camera_settings.set_cam_settings_from_cli()

cv2.createTrackbar('cam', 'Trackbars', 0, 5, on_trackbar_2)

while True:
    kaamera.set_frame()
    img = kaamera.get_frame_bgr()
    img_hsv = kaamera.get_frame_hsv()

    hsv, dev, target = get_tracks()

    varv = get_target(target)
    #vastavalt targetile kirjutame yle vaartused
    if target == eelmine_target:  # kui me ei ole muutnud targeti vaartus
        colors_hsv[varv] = hsv, dev
    else:  # kui me oleme muutnud targeti vaartus, tuleb lugeda sisse uued ning trackbarid uuendada
        hsv, dev = colors_hsv[varv]
        set_tracks(hsv, dev)

    eelmine_target = target

    HSV_array = np.array(hsv)
    HSV_dev_array = np.array(dev)

    lowerb = HSV_array - HSV_dev_array
    upperb = HSV_array + HSV_dev_array

    cv2.inRange(img_hsv, lowerb, upperb, dst)  # tekitab mustvalge pildi (dst) sobivatest punktidest

    kernel = np.ones((Pilt.BALL_MORPH_KERNEL_SIZE, Pilt.BALL_MORPH_KERNEL_SIZE), np.uint8)
    # dst = cv2.morphologyEx(dst, cv2.MORPH_CLOSE, kernel)  # dilate + erode

    #obj_rida, obj_veerg, laius, pikkus = getNelinurkAlumineKeskpunkt(img_hsv, hsv, dev)   #leiame objekti keskpunkti
    if target == 0:
        objekt = kaamera.pallid()
        if objekt:
            objekt = objekt[0]
    # elif target == 1:
    #     objekt = kaamera.varav_by_id(0)
    # elif target == 2:
    #     objekt = kaamera.varav_by_id(1)
    else:
        objekt = None
        kaamera.set_frame()

    if objekt:  # leidsime objekti yles, siis anname infot
        #cv2.putText(img, "Obj loc: " + str(obj_veerg) + "," + str(obj_rida), (50, 60), cv2.FONT_HERSHEY_SIMPLEX, 1, text_color) #kuvame obj asukoha
        # obj_veerg, obj_rida = int(min_rec[0][0]), int(min_rec[0][1])  # keskpunkt
        x, y, dx, dy = objekt
        obj_veerg, obj_rida = int(x + dx/2), int(y + dy)  # alumise aare keskpunkt
        kaugus, deeta = abi.get_kaugus(obj_veerg, obj_rida, 640, 480, 0.3, 0.3, math.radians(45))
        cv2.putText(img, "Obj loc: " + str(obj_veerg) + ", " + str(obj_rida), (50, 30), cv2.FONT_HERSHEY_SIMPLEX, 1,
                    text_color)  # kuvame obj asukoha
        cv2.putText(img, "kaugus: " + str(round(kaugus, 2)), (50, 60), cv2.FONT_HERSHEY_SIMPLEX, 1,
                    text_color)  # kuvame kauguse punktini
        cv2.putText(img, "suurus: " + str(round(dx*dy, 2)), (50, 120), cv2.FONT_HERSHEY_SIMPLEX, 1,
                    text_color)
        cv2.rectangle(img, (x,y), (x+dx,y+dy), rec_color, cv.CV_FILLED)
        #box = cv2.cv.BoxPoints(min_rec)
        #box = np.int0(box)
        #cv2.drawContours(img, [box], 0, rec_color, cv.CV_FILLED)

        #esitluse jaoks, keerame vasakule voi paremale
        if abs(deeta) < math.radians(2.5):
            cv2.putText(img, "mine otse", (50, 90), cv2.FONT_HERSHEY_SIMPLEX, 1, text_color)
        elif deeta < 0:
            cv2.putText(img, "keera vasakule {0} kraadi".format(round(math.degrees(abs(deeta)), 1)), (50, 90),
                        cv2.FONT_HERSHEY_SIMPLEX, 1, text_color)
        else:
            cv2.putText(img, "keera paremale {0} kraadi".format(round(math.degrees(abs(deeta)), 1)), (50, 90),
                        cv2.FONT_HERSHEY_SIMPLEX, 1, text_color)

    else:  # ei leidnud palli, esitluse jaoks
        cv2.putText(img, "Ei leidnud objekti. Keera!", (50, 90), cv2.FONT_HERSHEY_SIMPLEX, 1, text_color)

    #esitluse jaoks, joon keskele
    cv2.line(img, (kaamera.px_width//2, 0), (kaamera.px_width//2, kaamera.px_height-1), rec_color)
    cv2.line(img, (0, kaamera.px_height//2), (kaamera.px_width - 1, kaamera.px_height//2), rec_color)

    ys = np.arange(1, int(kaamera.px_height * 0.9))
    xs = np.linspace(kaamera.px_width // 2, kaamera.px_width // 2 - kaamera.px_width // 18, num=len(ys)).astype('uint16')  # vasakult
    xs1 = np.linspace(kaamera.px_width // 2, kaamera.px_width // 2 + kaamera.px_width // 18, num=len(ys)).astype('uint16')  # vasakult

    img[ys, xs] = [0, 0, 0]
    img[ys, xs1] = [0, 0, 0]
    dst[ys, xs] = 255
    dst[ys, xs1] = 255

    if kaamera.pall_vasakul_joonel():
        cv2.putText(img, "vasakul joonel", (50, 150), cv2.FONT_HERSHEY_SIMPLEX, 1, text_color)
    if kaamera.pall_paremal_joonel():
        cv2.putText(img, "paremal joonel", (50, 150), cv2.FONT_HERSHEY_SIMPLEX, 1, text_color)

    #mustvalge pildi kuvamiseks (testimisel)
    cv2.imshow("0:orange  1:yellow  2:blue  3:black  4:white  5:green", dst)

    cv2.setMouseCallback("s - save, q - valju", on_mouse, 0)

    cv2.imshow("s - save, q - valju", img)

    ch = cv2.waitKey(5)
    if ch == 1048689 or ch == 1048603 or ch == ord('q'):  # 'q' or 'Esc'
        break
    elif ch == 1048691 or ch == ord('s'):  # 's'
        with open(COL_FILE, 'w') as f:
            pickle.dump(colors_hsv, f)

del kaamera

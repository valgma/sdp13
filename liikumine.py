from math import cos, radians, degrees
import abi
import serial
import time


class Liikumine:

    def __init__(self, yhendused):
        self.s_vasak = yhendused[0]
        self.s_parem = yhendused[1]
        self.s_taga = yhendused[2]
        self.s_vasak.write('dr1\n')
        self.s_parem.write('dr1\n')
        self.s_taga.write('dr1\n')
        self.k_vasak = 0
        self.k_parem = 0
        self.k_taga = 0
        self.viga = False

    def __del__(self):
        try:
            self.otse(0)
            self.s_vasak.close()
            self.s_parem.close()
            self.s_taga.close()
        except Exception:
            print "Ei saanud yhendusi kinni"

    def soida(self, suund, kiirus, keere):
        self.liiguta(self.arvuta(suund, kiirus, keere))

    def arvuta(self, suund, kiirus, keere):
        #print("info: ", suund, kiirus, keere)
        vasak = int(round(kiirus * (cos(radians(150 - degrees(suund)))) + keere))
        parem = int(round(kiirus * (cos(radians(30 - degrees(suund)))) + keere))
        taga = int(round(kiirus * (cos(radians(270 - degrees(suund)))) + keere))
        #print("kiirused: ", vasak, parem, taga)
        #print("viga: ", self.viga)
        return [vasak, parem, taga]

    def liiguta(self, kiirused):  # kirjutab rataste mootorite sisesndisse kiirused
        try:
            self.s_vasak.write('sd' + str(kiirused[0]) + '\n')
            self.k_vasak = kiirused[0]
        except Exception as e:
            print ("viga vasaku ratta liigutamisel")
            print e.message

        try:
            self.s_parem.write('sd' + str(kiirused[1]) + '\n')
            self.k_parem = kiirused[1]
        except Exception as e1:
            print e1.message
            print ("viga parema ratta liigutamisel")

        try:
            self.s_taga.write('sd' + str(kiirused[2]) + '\n')
            self.k_taga = kiirused[2]
        except Exception as e2:
            print e2.message
            print ("viga tagumise ratta liigutamisel")

    def otse(self, kiirus):
        self.soida(0, kiirus, 0)

    def keera_nurgaga(self, nurk, kiirus):  # keerab nurgaga m2rgitud suuna poole etteantud kiirusega
        kohandatud_keeramise_kiirus = abi.keera(nurk, kiirus)
        self.soida(0, 0, kohandatud_keeramise_kiirus)

    def set_viga(self, viga):
        self.viga = viga

    def keera_ymber_dribbleri(self, kiirus):
        self.liiguta([0.2 * kiirus, 0.2 * kiirus, kiirus])

    def keera_ymber_dribbleri_nurgaga(self, nurk, kiirus):
        if nurk < 0:
            koh = kiirus
        else:
            koh = -kiirus
        #print("koh:", koh)
        self.liiguta([0.2 * koh, 0.2 * koh, koh])

    def keera_konst_paremale(self, kiirus):
        self.liiguta([kiirus, kiirus, kiirus])
        #print "paremale"

    def keera_konst_vasakule(self, kiirus):
        self.liiguta([-kiirus, -kiirus, -kiirus])
        #print "vasakule"

    def keera_konst_paremale_dr(self, kiirus):
        self.liiguta([0.2*kiirus, 0.2*kiirus, kiirus])
        #print "paremale dr"

    def keera_konst_vasakule_dr(self, kiirus):
        self.liiguta([-0.2*kiirus, -0.2*kiirus, -kiirus])
        #print "vasakule dr"

    def soida_vasakule(self, kiirus):
        self.liiguta(self.arvuta(90, kiirus, 0))

    def soida_paremale(self, kiirus):
        self.liiguta(self.arvuta(270, kiirus, 0))

    def paremale_varava_poole(self):  # test!
        self.liiguta(self.arvuta(270, 40, -10))

    def vasakule_varava_poole(self):  # test!
        self.liiguta(self.arvuta(90, 40, 10))
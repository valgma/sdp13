# suhteliselt algne kontrollerist sisendi lugemine
import pygame
import time

pygame.init()

#sisendi id, kohapeal teoorias vaja muuta
joyid = 0

stick = pygame.joystick.Joystick(joyid)
stick.init()
print stick.get_name()


def seis():
    #rumblepad
    #raw = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    #xbox 360
    raw = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    j = 0
    pygame.event.pump()

    for i in range(0, stick.get_numaxes()):
        raw[j] = stick.get_axis(i)
        j += 1
    for i in range(0, stick.get_numbuttons()):
        raw[j] = stick.get_button(i)
        j += 1

    return raw


def test():
    while True:
        time.sleep(0.1)
        print seis()

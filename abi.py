import math
import serial
import subprocess


def keskel(kujund, frame_width, frame_height, lotk):  # tagastab, kas kujund (pall, varav) pildi keskel voi mitte
    # kujund on siis boundingRect (x_yl_vasak, y_yl_vasak, laius, korgus)
    kujund_kaugus, nurk = kaugus(kujund, frame_width, frame_height)
    # x = kujund[0]   # ylemise vasaku nurga rea koord
    # laius = kujund[2]
    # ridu_kokku = frame_width  # mitu pixlit on pildi laius
    # kujund_kesk_rida = x + laius // 2  # leiame kujundi keskpunkti
    # print kujund_kesk_rida
    if abs(nurk) < math.radians(lotk):
        return True
    else:
        return False


def get_kaugus(obj_veerg, obj_rida, px_width, px_height, cam_h, cam_l, alpha):
    """ Tagastab objekti kauguse meetrites roboti esiotsast.
    obj_veer, obj_rida - punkti koordinaadid
    px_veerg, px_rida - pildi mootmed pixlites, resolutsioon
    cam_h - kaamera korgus meetrites
    cam_l - kaamera kaugus roboti esiotsast
    alpha - nurk normaali ning kaamera pildi alumise aare vahe, surnud ala nurk"""

    obj_veerg = float(obj_veerg)
    obj_rida = float(obj_rida)
    fov_v = math.radians(45)  # kaamera vertikaalne vaatevali
    #fov_h = math.radians(60)  # horisontaalne vaatevali
    fov_h = math.radians(75)  # laiekraan reso
    fii = fov_v * (1 - obj_rida / px_height)        # nurk pildi alumise aare ning punkti vahel
    deeta = fov_h * (obj_veerg / px_width - 0.5)    # nurk vertikaalse keskjoone ning punkti vahel, vasakul negatiivne
    dx = cam_h * math.tan(alpha + fii) - cam_l      # kaugus otsesuunas

    try:
        kaugus = dx / math.cos(deeta)                   # kaugus punktini
    except ZeroDivisionError:
        kaugus = 6.5

    return kaugus, deeta  # tagastame kauguse ningkadumiseAeg nurga palju poorama peame


def kaugus(kujund, px_width, px_height):  # kujundi kaugus, sobiva punkti kujudil leiame ise
    cam_h = 0.27
    cam_l = 0.20
    alpha = math.radians(44)
    obj_veerg = kujund[0] + kujund[2] // 2  # boundingRect keskpunkt
    obj_rida = kujund[1] + kujund[3]   # votame alumise aare hetkel

    return get_kaugus(obj_veerg, obj_rida, px_width, px_height, cam_h, cam_l, alpha)


def noksu_f(x, periood, n1, y0, dy):
    """kiiruse muutmiseks keeramisel, hetkel lineaarne
    x - argument, idees mingi counter, t2isarv
    periood - mitu tsyklit periood kestab
    n1 - kaua oleme y0 kiirusel (ylejaanud aja jooksul saavutame max kiiruse ning tagasi y0)
    y0 - base kiirus
    dy - suurim kiiruse lisa, y_max = y0 + dy
    """
    x = float(x % periood)
    #print(x)
    n2 = n1 + (periood - n1) / 2  # kiirenduse/aeglustuse aeg
    if x < n1:
        return y0
    elif x < n2:  # tous
        return int(y0 + dy * (x - n1) / (n2 - n1))
    else:  # langus
        return int(y0 + dy * (1 - (x - n2) / (periood - n2)))


def start():
    return True


# def change_dribbler_state(port):
#     port.write("m\n")
#
# def tribbler_stop(port):
#     port.write("ts\n")
#
# def tribbler_go(port):
#     port.write("tg\n")

def suus(pall, frame_width, frame_height):  # kontrollib, kas pall on h2sti pildi all (ning keskel)
    #pall siis ikka boundingRect
    pixleid_alt_servast = 120
    if pall:  # kontrollime, kas ikka saime midagi
        # palli ylemine nurk on alumisest servast vahem, kui pixleid_alt_servast
        # kaugus keskpunktist (laiuses) on vaiksem kui viiendik ekraani
        if pall[1] > (frame_height - pixleid_alt_servast) and abs(pall[0] + pall[2] / 2 -
                        frame_width / 2) < (frame_width / 10):
            #TODO abi.suus() - testida sobiv pixlite arv korguse jaoks
            return True
    return False


def kas_pall_joonest_allpool(pall_x, pall_y, x1, y1, x2, y2, frame_width):
    if abs(x1 - x2) > 0.1:  # punktid pole sama vertikaalse joone peal
        sirge_y = y1 + (y2 - y1) / float(x2 - x1) * (pall_x - x1)  # sirge vorrand, y=f(pall_x)
        return pall_y < sirge_y  # palli y-koord < leitud sirge vorrandi y-koord f(pall_x)
    else:  # vordleme, kas joone voi palli x-koord on keskpunktile lahemal, kui palli oma, siis OK
        return abs(x1 - frame_width / 2) > abs(pall_x - frame_width / 2)


def keera(nurk, kiirus):  # nurk - rad; negatiive - vasakule; pos - paremale
    kordaja = 1.0  # vasakule pooramiseks kiirus positiivne

    if nurk > 0:  # paremale kiirus negatiivne
        kordaja = -1.0

    if abs(nurk) < 0.4:  # kui nurk piisavalt vaike, vahendame keeramise kiirust kuni pooleni
        return kordaja * kiirus * (0.2 + abs(nurk) * 2)
    else:
        return kordaja * kiirus


def keera_palliga(nurk, kiirus):
    kordaja = 1.0  # vasakule pooramiseks kiirus positiivne

    if nurk > 0:  # paremale kiirus negatiivne
        kordaja = -1.0

    if abs(nurk) < 0.8:  # kui nurk piisavalt vaike, vahendame keeramise kiirust kuni pooleni
        return kordaja * kiirus * (0.1 + math.sin(abs(nurk)))
    else:
        return kordaja * kiirus


def kiirus(kaugus, nurk, maksimum):  # soltuvalt kaugusest objektini ning nurgast, tagastab soidu kiiruse
    # magik = abs(math.degrees(nurk) / kaugus)
    # if magik < 1:
    #     magik = 1.0
    #
    # return int(1.5 * maksimum / magik)

    # ei kasuta nurka hetkel, enamasti paneme ikka otse peale
    # 0.4 meetri kaugusel hakkame aeglustama, kuni 10-ni
    aeglustus_dx = 1
    min_kiirus = float(25)
    if kaugus > aeglustus_dx:
        return maksimum
    else:
        return int(min_kiirus + math.sin(kaugus / aeglustus_dx) * (maksimum - min_kiirus))


def pall_dribleris(port):
    return get_lyliti_state(port, 1)
    # try:
    #     port.write('gb\n')
    #     rida = port.readline()
    #     info = int(rida.split(':')[1][:-2])
    # except:
    #     print "Viga andurist kysimisel"
    #     return False
    #
    # if info == 1:
    #     return True
    # else:
    #     return False

def get_lyliti_state(port, state): #kysib mootoriplaadilt lyliti asendit; port = mootoriplaadi yhendus, state = mis asendit True-ks lugeda (0 - lyliti lahti/1 lyliti kinni)
    try:
        port.write('gb\n')
        rida = port.readline()
        info = int(rida.split(':')[1][:-2])
    except:
        print "Viga mootoriplaadilt info kysimises"
        return False

    if info == state:
        return True
    else:
        return False

def yhendused(ided):
    vasak = ided[0]
    parem = ided[1]
    taga = ided[2]
    coil = ided[3]
    olemas = 0
    s_vasak = None
    s_parem = None
    s_taga = None
    s_coil = None

    while olemas < 4:
        try:
            pordidpre = subprocess.check_output("ls /dev/ttyACM*", shell=True)
            pordid = pordidpre.split('\n')
            pordid.pop()
            print pordid
            if not (len(pordid) == 4):
                print "Kolm mootorit v6i coil ei ole taga!"
                pordid = False
        except Exception:
            print "Mingi jama yhendustega"
            pordid = False
            break
        if pordid:
            for pordo in pordid:
                print "a"
                ser = serial.Serial(port=pordo, baudrate=115200, timeout=0.1)
                print "b"
                ser.flush()
                print "c"
                ser.write('?\n')
                print "d"
                numberpre = ser.readline()
                print numberpre
                if numberpre.find("dis") > -1 or numberpre.find(",") > -1:
                    s_coil = ser
                    print "coil"
                    olemas += 1

                else:
                    try:
                        number = int(numberpre.split(':')[1][:-2])
                        if number == vasak:
                            if s_vasak:
                                print ("vasaku mootori id kattumine")
                                break
                            s_vasak = ser
                            print "vasak"
                            olemas += 1
                        elif number == parem:
                            if s_parem:
                                print ("parema mootori id kattumine")
                                break
                            s_parem = ser
                            print "parem"
                            olemas += 1
                        elif number == taga:
                            if s_taga:
                                print ("tagumise mootori id kattumine")
                                break
                            s_taga = ser
                            print "taga"
                            olemas += 1
                        else:
                            pass
                    except Exception:
                        print "mingit plaati ei suutnud tuvastada"
            if not olemas == 4:
                print "Ei saanud yhendusi!"
                print olemas
                olemas = 0
                try:
                    s_vasak.close()
                    s_vasak = None
                    print "vasak maas"
                except Exception:
                    pass
                try:
                    s_parem.close()
                    s_parem = None
                    print "parem maas"
                except Exception:
                    pass
                try:
                    s_taga.close()
                    s_taga = None
                    print "taga maas"
                except Exception:
                    pass
                try:
                    s_coil.close()
                    s_coil = None

                    print "coil maas"
                except Exception:
                    pass

    return [s_vasak, s_parem, s_taga, s_coil]

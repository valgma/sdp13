from pilt import Pilt
import camera_settings
import cv2


# lisame trackbari v2rava valimiseks
def on_trackbar(tb_value):
    global goal_id
    try:
        goal_id = tb_value
        kaamera.load_varav_hsv(goal_id)
        print 'varava ID:', goal_id
    except:
        print 'Viga varava ID muutmisega'

def on_trackbar_2(tb_value):
    camera_settings.set_cam_settings_from_cli()

goal_id = 1
kaamera = Pilt(0, goal_id)

switch = '0 : Kollane\n1 : Sinine'
switch1 = 'Trigger cam settings'
cv2.namedWindow('Kaamera pilt')
cv2.createTrackbar(switch, 'Kaamera pilt', goal_id, 1, on_trackbar)
_tb1 = 0
cv2.createTrackbar(switch1, 'Kaamera pilt', _tb1, 1, on_trackbar_2)
# trackari osa lopeb
#camera_settings.set_cam_settings_from_cli()

try:
    while True:
        pallid = kaamera.pallid()
        #bgr_img = cv2.cvtColor(kaamera.get_frame_hsv(), cv2.COLOR_HSV2BGR)
        bgr_img = kaamera.get_frame_bgr()
        cv2.putText(bgr_img, "Varava ID: " + str(goal_id), (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, [0, 0, 255], 2)
        cv2.imshow('Kaamera pilt', bgr_img)
        ch = cv2.waitKey(5)
        if ch == 1048689 or ch == 1048603 or ch == ord('q'):  # 'q' or 'Esc'
            break

finally:
    del kaamera
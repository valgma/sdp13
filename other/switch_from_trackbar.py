import cv2

goal_id = 0


def on_trackbar(x):
    print 'new value:', x


switch = '0:K   \n1:S'
cv2.namedWindow('image')
cv2.createTrackbar(switch, 'image', goal_id, 1, on_trackbar)

while True:
    ch = cv2.waitKey(5)
    if ch == 1048689 or ch == 1048603 or ch == ord('q'):  # 'q' or 'Esc'
        break

cv2.destroyAllWindows()

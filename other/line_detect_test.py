import cv2
import numpy as np

img = cv2.imread('field.png')
#img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
cv2.imwrite('gray.jpg', gray)

ret, edges = cv2.threshold(gray, 50, 255, cv2.THRESH_BINARY_INV)
cv2.imwrite('edges.jpg', edges)

kernel = np.ones((5, 5), np.uint8)
# edges = cv2.erode(edges, kernel)
# edges = cv2.dilate(edges, kernel)
edges = cv2.morphologyEx(edges, cv2.MORPH_OPEN, kernel)
cv2.imwrite('edges_after_erode.jpg', edges)

minLineLength = 100
maxLineGap = 10
lines = cv2.HoughLinesP(edges, 1, np.pi / 180, 100, minLineLength, maxLineGap)
for x1, y1, x2, y2 in lines[0]:
    cv2.line(img, (x1, y1), (x2, y2), (0, 0, 255), 2)
    print "line", x1, y1, x2, y2

cv2.imwrite('field1.jpg', img)